package com.aptitudeguru.dashboard;

public class Resultvl extends Result {
	
	@Override
	protected void fetchQuestionResult(int questionID, String category, int answer) {
		QuestionTable q = db.getQuestion(questionID, category, new VLTable());
		String j = q.getQues();
		String opt1 = q.getOption1();
		String opt2 = q.getOption2();
		String opt3 = q.getOption3();
		String opt4 = q.getOption4();
		String sol = q.getSol();
		t1.setText(j);
		t2.setText("1." + opt1);
		t3.setText("2." + opt2);
		t4.setText("3." + opt3);
		t5.setText("4." + opt4);
		String j1 = answer + "";
		if (answer == 0)
			j1 = "-";
		if (sol.equalsIgnoreCase("a"))
			sol = 1 + "";
		else if (sol.equalsIgnoreCase("b"))
			sol = 2 + "";
		else if (sol.equalsIgnoreCase("c"))
			sol = 3 + "";
		else if (sol.equalsIgnoreCase("d"))
			sol = 4 + "";
		else {
		}
		t6.setText("Selected Answer " + j1 + "");
		t7.setText("Correct Answer " + sol + "" + "\n" + "\t\t");
		
	}
}
