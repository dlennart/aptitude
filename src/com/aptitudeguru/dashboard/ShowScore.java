package com.aptitudeguru.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidhive.dashboard.R;

import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ShowScore extends AptitudeBaseActivity {

	TextView t1;
	DatabaseHandler db = new DatabaseHandler(this);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_score);
		int incorrect = 0;
		createMainMenu();

		Bundle bundle = getIntent().getExtras();
		final int useranswer[] = bundle.getIntArray("score");
		final int solution[] = bundle.getIntArray("givenans");
		final String cat = bundle.getString("category");
		final int allQuestionsId[] = bundle.getIntArray("allid");
		final String timetaken = bundle.getString("tt");
		TextView timetak = (TextView) findViewById(R.id.textView5);
		TextView incorr = (TextView) findViewById(R.id.textView3);
		TextView unattem = (TextView) findViewById(R.id.textView4);
		timetak.append(" " + timetaken);
		int score = 0;

		ArrayList<Button> scoreButtons=createScoreButtons();

		int [] total=calculateTotalScore(solution,useranswer);
		score=total[0];
		incorrect=total[1];
		
		for (int i = 0; i < scoreButtons.size(); i++) {
			int colourID = colourAssignment(solution[i], useranswer[i]);
			scoreButtons.get(i).setBackgroundColor(colourID);
			
			scoreButtons.get(i).setTag(i);
			scoreButtons.get(i).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					int i = (Integer) view.getTag();
					String id = allQuestionsId[i] + "";
					String current = i + "";
					Intent intent = new Intent(getApplicationContext(),
							Result.class);
					intent.putExtra("ComingFrom", id);
					intent.putExtra("current", current);
					intent.putExtra("Category", cat);
					intent.putExtra("yourans", useranswer);
					intent.putExtra("givenans", solution);
					intent.putExtra("allid", allQuestionsId);
					// setResult(Result_OK, intent);
					startActivity(intent);

				}
			});
		}

		t1 = (TextView) findViewById(R.id.textView2);
		t1.append(score + "");

		DateFormat dateFormat = new SimpleDateFormat("dd/MM  HH:mm");
		Date date = new Date();
		String dateStr = dateFormat.format(date);
		String dateDB = dateStr + "";
		String scoreDB = score + "";
		
		db.addSbtable(new sbtable("quants", cat, dateDB, scoreDB, timetaken));
		
		incorr.append(incorrect + "");
		int unattemNun = 20 - (score + incorrect);
		unattem.append(unattemNun + "");

	}
	
	public int colourAssignment(int solution, int userAnswer)
	{
		if (userAnswer == 0) {
			return Color.BLUE;
		} else if (userAnswer == solution) {
			return Color.GREEN;
		} else {
			return Color.RED;
		}
	}
	
	
	public int[] calculateTotalScore(int[] solutions,int[] userAnswers)
	{
		int [] totals= {0,0};
		for(int i=0;i<solutions.length;i++)
		{
			int colourID = colourAssignment(solutions[i], userAnswers[i]);
			
			if (colourID == Color.GREEN)
			{
				totals[0]++;
			}
			else if (colourID == Color.RED)
			{
				totals[1]++;
			}
		}
		return totals;
	}	
	
	
	public ArrayList<Button> createScoreButtons()
	{
		ArrayList<Button> scoreButtons = new ArrayList<Button>();
		scoreButtons.add((Button) findViewById(R.id.score1));
		scoreButtons.add((Button) findViewById(R.id.score2));
		scoreButtons.add((Button) findViewById(R.id.score3));
		scoreButtons.add((Button) findViewById(R.id.score4));
		scoreButtons.add((Button) findViewById(R.id.score5));
		scoreButtons.add((Button) findViewById(R.id.score6));
		scoreButtons.add((Button) findViewById(R.id.score7));
		scoreButtons.add((Button) findViewById(R.id.score8));
		scoreButtons.add((Button) findViewById(R.id.score9));
		scoreButtons.add((Button) findViewById(R.id.score10));
		scoreButtons.add((Button) findViewById(R.id.score11));
		scoreButtons.add((Button) findViewById(R.id.score12));
		scoreButtons.add((Button) findViewById(R.id.score13));
		scoreButtons.add((Button) findViewById(R.id.score14));
		scoreButtons.add((Button) findViewById(R.id.score15));
		scoreButtons.add((Button) findViewById(R.id.score16));
		scoreButtons.add((Button) findViewById(R.id.score17));
		scoreButtons.add((Button) findViewById(R.id.score18));
		scoreButtons.add((Button) findViewById(R.id.score19));
		scoreButtons.add((Button) findViewById(R.id.score20));
		return scoreButtons;
	}
}