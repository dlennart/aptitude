package com.aptitudeguru.dashboard;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.content.Intent;
import android.widget.Button;
import androidhive.dashboard.R;

public class Tutorialpage extends TutorialParent implements OnTouchListener {

	final static float STEP = 200;
	TextView mytv;
	float mRatio = 1.0f;
	int mBaseDist;
	float mBaseRatio;
	float fontsize = 13;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getPointerCount() == 2) {
			int action = event.getAction();
			int pureaction = action & MotionEvent.ACTION_MASK;
			if (pureaction == MotionEvent.ACTION_POINTER_DOWN) {
				mBaseDist = getDistance(event);
				mBaseRatio = mRatio;
			} else {
				float delta = (getDistance(event) - mBaseDist) / STEP;
				float multi = (float) Math.pow(2, delta);
				mRatio = Math.min(1024.0f, Math.max(0.1f, mBaseRatio * multi));
				mytv.setTextSize(mRatio + 13);
			}
		}
		return true;
	}

	int getDistance(MotionEvent event) {
		int dx = (int) (event.getX(0) - event.getX(1));
		int dy = (int) (event.getY(0) - event.getY(1));
		return (int) (Math.sqrt(dx * dx + dy * dy));
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return false;
	}

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) { 
	        // Activity was brought to front and not created, 
	        // Thus finishing this will get us to the last viewed activity 
	        finish(); 
	        return; 
	    } 
		setContentView(R.layout.tutorial_layout);
		createMainMenu();

		// Refactored code
		ArrayList<Button> tButtons = createTutorialButtons();
		
		String[] PDFs = {"ctuts.pdf", "cpp.pdf", "java.pdf", "OS.pdf",
				"database.pdf", "DataStructure.pdf", "HTML.pdf",
				"Interview.pdf" };
		
		
		tButtons.get(0).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {

				Intent i = new Intent(getApplicationContext(), Tquants.class);
				startActivity(i);
			}
		});

		createButtonsListeners(1,1,PDFs,tButtons);
		
	}
	
	public ArrayList<Button> createTutorialButtons()
	{
		ArrayList<Button> buttons = new ArrayList<Button>();

		buttons.add((Button) findViewById(R.id.btn_score_quants));
		buttons.add((Button) findViewById(R.id.btn_score_c));
		buttons.add((Button) findViewById(R.id.btn_score_cpp));
		buttons.add((Button) findViewById(R.id.btn_score_java));
		buttons.add((Button) findViewById(R.id.btn_score_os));
		buttons.add((Button) findViewById(R.id.btn_score_dbms));
		buttons.add((Button) findViewById(R.id.btn_score_datastructure));
		buttons.add((Button) findViewById(R.id.btn_score_html));
		buttons.add((Button) findViewById(R.id.btn_score_interview));
		return buttons;
	}
}

		