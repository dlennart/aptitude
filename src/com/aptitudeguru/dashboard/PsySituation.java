package com.aptitudeguru.dashboard;

import java.util.ArrayList;

public class PsySituation {

	int situationID;
	String situationText;
	String competency;
	ArrayList<PsyAnswer> answers;
	
	public PsySituation(int situationID, String situationText, String competency, ArrayList<PsyAnswer> answers) 
	{
		this.situationID = situationID;
		this.situationText = situationText;
		this.competency = competency;
		this.answers = answers;
	}

	public int getSituationID() {
		return situationID;
	}

	public String getSituationText() {
		return situationText;
	}

	public String getCompetency() {
		return competency;
	}

	public ArrayList<PsyAnswer> getAnswers() {
		return answers;
	}
	
}
