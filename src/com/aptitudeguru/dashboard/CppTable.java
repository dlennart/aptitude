package com.aptitudeguru.dashboard;

public class CppTable extends QuestionTable{
	
	public CppTable()
	{
		super();
		tableCat = "cpplanguage";
		key_questID = "cppid";
		key_quest = "cppques";
		key_cat = "cppcat";
	}
	
	public CppTable(int questionID, String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		super(questionID, question, questionCat, option1, option2, option3, option4, solution);
		tableCat = "cpplanguage";
		key_questID = "cppid";
		key_quest = "cppques";
		key_cat = "cppcat";
	}

	public CppTable(String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		super(question, questionCat, option1, option2, option3, option4, solution);
		tableCat = "cpplanguage";
		key_questID = "cppid";
		key_quest = "cppques";
		key_cat = "cppcat";
	}
	
}
