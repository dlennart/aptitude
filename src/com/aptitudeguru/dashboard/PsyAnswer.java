package com.aptitudeguru.dashboard;

public class PsyAnswer {

	int answerID;
	int priority;
	String answer;
	String comment;
	
	
	public PsyAnswer(int answerID, int priority, String answer, String comment)
	{
		this.answerID = answerID;
		this.priority = priority;
		this.answer = answer;
		this.comment = comment;
	}


	public int getAnswerID() {
		return answerID;
	}


	public int getPriority() {
		return priority;
	}


	public String getAnswer() {
		return answer;
	}


	public String getComment() {
		return comment;
	}
	
}
