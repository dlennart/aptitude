package com.aptitudeguru.dashboard;

import java.util.ArrayList;

//import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidhive.dashboard.R;

public class Quantitative extends AptitudeBaseActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) { 
	        // Activity was brought to front and not created, 
	        // Thus finishing this will get us to the last viewed activity 
	        finish(); 
	        return; 
	    } 
		setContentView(R.layout.news_feed_layout);
		createMainMenu();

		//Refactor of duplicate code into a single loop. Array quantCats could maybe be refactored to be passed
		//rather that static
		ArrayList<Button> quantButtons = createSubCategoryButtons();
		String[] quantCats = {"q1","q2","q4","q5","q6","q7","q8","q10","q11","q12","q13","q15","q16","q17"};

		for (int k = 0; k< quantButtons.size(); k++)
		{
			quantButtons.get(k).setTag(quantCats[k]);
			quantButtons.get(k).setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
							String cat = (String)view.getTag();
							Intent i = new Intent(getApplicationContext(), TestPage.class);
							i.putExtra("cat", cat);

							startActivity(i);
				}
			});
		}
	}

	public ArrayList<Button> createSubCategoryButtons()
	{
		ArrayList<Button> quantButtons = new ArrayList<Button>();
		quantButtons.add((Button) findViewById(R.id.btn_q1));
		quantButtons.add((Button) findViewById(R.id.btn_q2));
		quantButtons.add((Button) findViewById(R.id.btn_q4));
		quantButtons.add((Button) findViewById(R.id.btn_q5));
		quantButtons.add((Button) findViewById(R.id.btn_q6));
		quantButtons.add((Button) findViewById(R.id.btn_q7));
		quantButtons.add((Button) findViewById(R.id.btn_q8));
		quantButtons.add((Button) findViewById(R.id.btn_q10));
		quantButtons.add((Button) findViewById(R.id.btn_q11));
		quantButtons.add((Button) findViewById(R.id.btn_q12));
		quantButtons.add((Button) findViewById(R.id.btn_q13));
		quantButtons.add((Button) findViewById(R.id.btn_q15));
		quantButtons.add((Button) findViewById(R.id.btn_q16));
		quantButtons.add((Button) findViewById(R.id.btn_q17));
		return quantButtons;
	}
}
