package com.aptitudeguru.dashboard;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidhive.dashboard.R;

public class ShowAnswers extends AptitudeBaseActivity {

	DatabaseHandler db = new DatabaseHandler(this);
	TextView mainText;
	PsyScenario currentScenario;
	PsySituation currentSituation;
	ArrayList<PsyAnswer> answers;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) { 
	        // Activity was brought to front and not created, 
	        // Thus finishing this will get us to the last viewed activity 
	        finish(); 
	        return; 
	    } 
		setContentView(R.layout.answers);
		createMainMenu();
		Bundle bundle = getIntent().getExtras();
		String mostLikely = bundle.getString("mostLikely");
		String leastLikely = bundle.getString("leastLikely");
		
		
		currentScenario = db.getPsyScenario(1);
		currentSituation = currentScenario.getSituations().get(0);
		answers = currentSituation.getAnswers();
		
		PsyAnswer mostAnswer = convertAnswer(mostLikely);
		PsyAnswer leastAnswer = convertAnswer(leastLikely);
		displayAnswerInfo(mostAnswer, leastAnswer);	
	}

	private PsyAnswer convertAnswer(String answer) {
		if (answer.equalsIgnoreCase("A"))
		{
			return answers.get(0);
		}
		else if (answer.equalsIgnoreCase("B"))
		{
			return answers.get(1);
		}
		else if (answer.equalsIgnoreCase("C"))
		{
			return answers.get(2);
		}
		else
		{
			return answers.get(3);
		}
	}

	private void displayAnswerInfo(PsyAnswer most, PsyAnswer least) 
	{
		TextView competencyTV = (TextView) findViewById(R.id.competency);
		TextView mostAnswerTV = (TextView) findViewById(R.id.mostanswer);
		TextView mostCommentTV = (TextView) findViewById(R.id.mostcomment);
		TextView leastAnswerTV = (TextView) findViewById(R.id.leastanswer);
		TextView leastCommentTV = (TextView) findViewById(R.id.leastcomment);
		
		String competency = currentSituation.getCompetency();
		competencyTV.setText("Competency being measured: " +competency+ "\n");
		
		int mostColour = getAnswerColourCode(most.getPriority());
		mostAnswerTV.setText("\nOption Chosen:\n" + most.getAnswer());
		mostCommentTV.setText("\nComment for Option:\n" + most.getComment()+ "\n");
		mostCommentTV.setTextColor(mostColour);
		
		
		int leastColour = getAnswerColourCode(least.getPriority());
		leastAnswerTV.setText("\nOption Chosen:\n" + least.getAnswer());
		leastCommentTV.setText("\nComment for Option:\n" + least.getComment()+ "\n");
		leastCommentTV.setTextColor(leastColour);

		//mainText.setText(answersString);	
		
	}

	private int getAnswerColourCode(int priority) {
		switch (priority)
		{
			case 1: return Color.rgb(0, 204, 0);
			case 2: return Color.rgb(25, 163, 71);
			case 3: return Color.rgb(255, 204, 0);
			case 4: return Color.RED;
		}
		return 0;
	}

}
