package com.aptitudeguru.dashboard;

public class QuantsTable extends QuestionTable{

	public QuantsTable()
	{
		super();
		tableCat = "quants";
		key_questID = "quantsid";
		key_quest = "quantsques";
		key_cat = "quantscat";
	}
	
	public QuantsTable(int questionID, String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		super(questionID, question, questionCat, option1, option2, option3, option4, solution);
		tableCat = "quants";
		key_questID = "quantsid";
		key_quest = "quantsques";
		key_cat = "quantscat";
	}

	public QuantsTable(String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		super(question, questionCat, option1, option2, option3, option4, solution);
		tableCat = "quants";
		key_questID = "quantsid";
		key_quest = "quantsques";
		key_cat = "quantscat";
	}
	
}
