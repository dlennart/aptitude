package com.aptitudeguru.dashboard;

public class HTMLTable extends QuestionTable {

	public HTMLTable()
	{
		super();
		tableCat = "htmllanguage";
		key_questID = "hmtlid";
		key_quest = "hmtlques";
	}
}
