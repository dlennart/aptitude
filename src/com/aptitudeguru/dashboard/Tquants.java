package com.aptitudeguru.dashboard;

import java.util.ArrayList;

import android.os.Bundle;
import android.widget.Button;
import androidhive.dashboard.R;

public class Tquants extends TutorialParent {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_feed_layout);
		createMainMenu();
		
		// Refactored, ~700-800 lines of code deleted
		ArrayList<Button> tButtons = new ArrayList<Button>();
		tButtons.add((Button) findViewById(R.id.btn_q1));
		tButtons.add((Button) findViewById(R.id.btn_q2));
		tButtons.add((Button) findViewById(R.id.btn_q4));
		tButtons.add((Button) findViewById(R.id.btn_q5));
		tButtons.add((Button) findViewById(R.id.btn_q6));
		tButtons.add((Button) findViewById(R.id.btn_q7));
		tButtons.add((Button) findViewById(R.id.btn_q8));
		tButtons.add((Button) findViewById(R.id.btn_q10));
		tButtons.add((Button) findViewById(R.id.btn_q11));
		tButtons.add((Button) findViewById(R.id.btn_q12));
		tButtons.add((Button) findViewById(R.id.btn_q13));
		tButtons.add((Button) findViewById(R.id.btn_q15));
		tButtons.add((Button) findViewById(R.id.btn_q16));
		tButtons.add((Button) findViewById(R.id.btn_q17));
		
		
		String[] PDFs = { "sec1.pdf", "sec2.pdf", "sec4.pdf",
				"sec5.pdf", "sec6.pdf", "sec7.pdf", "sec8.pdf", "sec10.pdf", "sec11.pdf", "sec12.pdf", "sec13.pdf", "sec15.pdf", "sec16.pdf", "sec17.pdf" };
		
		createButtonsListeners(0,0,PDFs,tButtons);
		/*for (int k = 0; k < tButtons.size(); k++){
			tButtons.get(k).setTag(PDFs[k]);
			tButtons.get(k).setOnClickListener(new View.OnClickListener() {
			
				@Override
				public void onClick(View view) {
					String pdf = (String) view.getTag();

					AssetManager assetManager = getAssets();
					InputStream in = null;
					OutputStream out = null;
					File file = new File(getFilesDir(), pdf);
					try {
						in = assetManager.open(pdf);
						out = openFileOutput(file.getName(),
								Context.MODE_WORLD_READABLE);

						copyFile(in, out);
						in.close();
						in = null;
						out.flush();
						out.close();
						out = null;
					} catch (Exception e) {
						Log.e("tag", e.getMessage());
					}

					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(
							Uri.parse("file://" + getFilesDir() + "/"+ pdf),
							"application/pdf");

					startActivity(intent);
				}

				private void copyFile(InputStream in, OutputStream out)
						throws IOException {
					byte[] buffer = new byte[1024];
					int read;
					while ((read = in.read(buffer)) != -1) {
						out.write(buffer, 0, read);
					}
		

				}
			});
		}*/
	}
}
		
		
