package com.aptitudeguru.dashboard;

public class CTable extends QuestionTable {

	public CTable()
	{
		super();
		tableCat = "clanguage";
		key_questID = "cid";
		key_quest = "cques";
		key_cat = "ccat";
	}
	
	public CTable(int questionID, String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		super(questionID, question, questionCat, option1, option2, option3, option4, solution);
		tableCat = "clanguage";
		key_questID = "cid";
		key_quest = "cques";
		key_cat = "ccat";
	}

	public CTable(String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		super(question, questionCat, option1, option2, option3, option4, solution);
		tableCat = "clanguage";
		key_questID = "cid";
		key_quest = "cques";
		key_cat = "ccat";
	}

}
