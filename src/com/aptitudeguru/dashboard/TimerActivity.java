package com.aptitudeguru.dashboard;

import android.app.Activity;

import android.view.View.OnClickListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidhive.dashboard.R;

public class TimerActivity  extends Activity implements OnClickListener{
	
	 private CountDownTimer countDownTimer;
	 private boolean timerHasStarted = false;
	 private Button startB;
	 public TextView text;
	 private long startTime;
	 private long interval;
	
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.activity_timer);
	  text = (TextView) this.findViewById(R.id.timer);
	  CountDownTimerCreation((120 * 1000), (1 * 1000), countDownTimer);
	  text.setText(text.getText() + String.valueOf(startTime / 1000));
	 }
	
	 public CountDownTimer CountDownTimerCreation(long startTime2, long interval2, CountDownTimer t) {
		 startTime = startTime2;
		 interval = interval2;
		 t = new MyCountDownTimer(startTime, interval);
		 countDownTimer = t;
		 return t;
	}
	 
	public long getStartTime()
	{
		return startTime;
	}
	
	public long getInterval()
	{
		return interval;
	}
	 
	 @Override
	public void onClick(View v) {
	  if (!timerHasStarted) {
	   countDownTimer.start();
	   timerHasStarted = true;
	   startB.setText("STOP");
	  } else {
	   countDownTimer.cancel();
	   timerHasStarted = false;
	   startB.setText("RESTART");
	  }
	 }
	 
	 public class MyCountDownTimer extends CountDownTimer {
	  public MyCountDownTimer(long startTime, long interval) {
	   super(startTime, interval);
	  }

	  @Override
	  public void onFinish() {
	   text.setText("Time's up!");
	  }
	
	  @Override
	  public void onTick(long millisUntilFinished) {
	   text.setText("" + millisUntilFinished / 1000);
	  }

	 }
}
