package com.aptitudeguru.dashboard;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpMatching {

	private String expressionText = "";
	
	public RegExpMatching(String expression)
	{
		this.expressionText = expression;
	}
	
	public void setExpression(String expression)
	{
		this.expressionText = expression;
	}
	
	public boolean expressionMatch(String toMatch) {
		
		Pattern p = Pattern.compile("(.)*(" + expressionText + ")(.)*", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(toMatch);
		return m.matches();
	}

	public String[] expressionSplit(String toSplit) {
		Pattern p = Pattern.compile("(" + expressionText + ")", Pattern.CASE_INSENSITIVE);
		return p.split(toSplit);
	}
	
	public String expressionReplace(String toReplace, String replacement)
	{
		Pattern p = Pattern.compile("(" + expressionText + ")", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(toReplace);
		String replaced = m.replaceAll(replacement);
		return replaced;
	}
	

}
