package com.aptitudeguru.dashboard;

public class QuestionTable {

	int questionID;
	String question;
	String questionCat;
	String option1;
	String option2;
	String option3;
	String option4;
	String solution;
	
	protected String tableCat;
	protected String key_questID;
	protected String key_quest;
	protected String key_cat;
	StringReplacer sr = new StringReplacer();

	public QuestionTable() {

	}
	
	public QuestionTable(int questionID, String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		this.questionID = questionID;
		this.question = question;
		this.questionCat = questionCat;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.solution = solution;
	}

	public QuestionTable(String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		this.question = question;
		this.questionCat = questionCat;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.solution = solution;
	}
	
	public int getID() {
		return this.questionID;
	}


	public void setID(int questionID) {
		this.questionID = questionID;
	}

	public String getTableCat()
	{
		return this.tableCat;
	}
	
	public void setTableCat(String tableCat)
	{
		this.tableCat = tableCat;
	}

	public String getQues() {
		return sr.convertStringWithreplacement(this.question);
	}

	
	public void setQues(String question) {
		this.question = question;
	}

	
	public String getCat() {
		return this.questionCat;
	}

	
	public void setCat(String questionCat) {
		this.questionCat = questionCat;
	}

	
	public String getOption1() {
		return sr.convertStringWithreplacement(this.option1);
	}

	public String getOption2() {
		return sr.convertStringWithreplacement(this.option2);
	}

	public String getOption3() {
		return sr.convertStringWithreplacement(this.option3);
	}

	public String getOption4() {
		return sr.convertStringWithreplacement(this.option4);
	}

	public String getSol() {
		return sr.convertStringWithreplacement(this.solution);
	}

	public void setSol(String solution) {
		this.solution = solution;
	}

	
	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}

	public void setOption4(String option4) {
		this.option4 = option4;
	}
	
	public String getKeyQuestID()
	{
		return key_questID;
	}
	
	public String getKeyQuest()
	{
		return key_quest;
	}
	
	public String getKeyCat()
	{
		return key_cat;
	}

	public String getQuesOrig() {
		return question;
	}

	public String getOption1Orig() {
		return option1;
	}

	public String getOption2Orig() {
		return option2;
	}

	public String getOption3Orig() {
		return option3;
	}

	public String getOption4Orig() {
		return option4;
	}

	public String getSolOrig() {
		return solution;
	}

}
