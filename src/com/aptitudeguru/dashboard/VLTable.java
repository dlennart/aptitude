package com.aptitudeguru.dashboard;

public class VLTable extends QuestionTable{
	
	public VLTable()
	{
		super();
		tableCat = "vl";
		key_questID = "vlid";
		key_quest = "vlques";
		key_cat = "vlcat";
	}
	
	public VLTable(int questionID, String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		super(questionID, question, questionCat, option1, option2, option3, option4, solution);
		tableCat = "vl";
		key_questID = "vlid";
		key_quest = "vlques";
		key_cat = "vlcat";
	}

	public VLTable(String question, String questionCat,
			String option1, String option2, String option3, String option4,
			String solution) {
		super(question, questionCat, option1, option2, option3, option4, solution);
		tableCat = "vl";
		key_questID = "vlid";
		key_quest = "vlques";
		key_cat = "vlcat";
	}

}
