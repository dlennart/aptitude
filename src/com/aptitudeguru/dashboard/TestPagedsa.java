package com.aptitudeguru.dashboard;

import java.util.List;
import android.view.View.OnClickListener;


public class TestPagedsa extends TestPage implements OnClickListener {

	@Override
	protected void fetchAllQuestions(String category) {
		int g = 0;
		List<QuestionTable> questionList = db.getAllQuestionsShort(category, new DSATable());
		for (QuestionTable cn : questionList) {

			if (g == 38)
				break;
			else {
				int currentID = cn.getID();
				String sol1 = cn.getSol();
				int sol = 0;
				if (sol1.equalsIgnoreCase("a"))
					sol = 1;
				else if (sol1.equalsIgnoreCase("b"))
					sol = 2;
				else if (sol1.equalsIgnoreCase("c"))
					sol = 3;
				else
					sol = 4;
				// int j=r.nextInt(2);
				// int k=(count+j+1);
				// count=k;
				allQuestionsFromDatabase[g] = currentID;
				allAnswersFromDatabase[g] = sol;
				g++;
			}
		}
	}

	@Override
	protected void fetchQuestion(int questionID, String category) {
		QuestionTable q = db.getQuestionShort(questionID, category, new DSATable());
		
		String j = q.getQues();
		questionTextDisplay.setText(j);
		
		String opt1 = q.getOption1();
		String opt2 = q.getOption2();
		String opt3 = q.getOption3();
		String opt4 = q.getOption4();
		
		optionBtn1.setText(opt1);
		optionBtn2.setText(opt2);
		optionBtn3.setText(opt3);
		optionBtn4.setText(opt4);
	}


	@Override
	protected void addToFav()
	{
		int val = questionIDs[currentQuestionNumber];
		QuestionTable q = db.getQuestion(val, category, new DSATable());
		String ques = q.getQues();
		String op1 = q.getOption1();
		String op2 = q.getOption2();
		String op3 = q.getOption3();
		String op4 = q.getOption4();
		String sol = q.getSol();
		db.addFav(new Favourite(ques, op1, op2, op3, op4, sol));
	}



}
