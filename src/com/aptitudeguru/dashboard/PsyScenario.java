package com.aptitudeguru.dashboard;

import java.util.ArrayList;

public class PsyScenario {

	int scenarioID;
	String scenarioText;
	ArrayList<PsySituation> situations;
	
	public PsyScenario(int scenarioID, String scenarioText, ArrayList<PsySituation> situations) {
		this.scenarioID = scenarioID;
		this.scenarioText = scenarioText;
		this.situations = situations;
	}
	public int getScenarioID() {
		return scenarioID;
	}
	public String getScenarioText() {
		return scenarioText;
	}
	public ArrayList<PsySituation> getSituations() {
		return situations;
	}
	
	
	
}
