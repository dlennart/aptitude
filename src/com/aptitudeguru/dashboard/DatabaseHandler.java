package com.aptitudeguru.dashboard;

import java.util.ArrayList;
import java.util.List;

//import com.example.taptap.DBH;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_NAME = "aptitudedatabase";

	private static final String TABLE_QUANTS = "quants";

	private static final String TABLE_CLANGUAGE = "clanguage";
	private static final String TABLE_CPPLANGUAGE = "cpplanguage";
	private static final String TABLE_JAVALANGUAGE = "javalanguage";
	private static final String TABLE_HTMLLANGUAGE = "htmllanguage";
	private static final String TABLE_VL = "vl";
	private static final String TABLE_OS = "os";
	private static final String TABLE_DBMS = "dbms";
	private static final String TABLE_DSA = "dsa";
	private static final String TABLE_TUTORIAL = "tutorial";
	private static final String TABLE_FAVOURITE = "favourite";
	private static final String TABLE_SBTABLE = "sbtable";
	private static final String TABLE_PUZZLETABLE = "PuzzleTable";

	private static final String KEY_PUZZLEID = "puzzleid";
	private static final String KEY_PUZZLEQUES = "puzzleques";
	private static final String KEY_PUZZLESOL = "sol";

	private static final String KEY_SBID = "sbid";
	private static final String KEY_SBSECTION = "sbsection";
	private static final String KEY_SBSUBSECTION = "sbsubsection";
	private static final String KEY_SBDATETIME = "sbdatetime";
	private static final String KEY_SBSCORE = "sbscore";
	private static final String KEY_SBTT = "sbtt";

	private static final String KEY_QUANTSID = "quantsid";
	private static final String KEY_QUANTSQUES = "quantsques";
	private static final String KEY_QUANTSCAT = "quantscat";
	private static final String KEY_OPTION1 = "option1";
	private static final String KEY_OPTION2 = "option2";
	private static final String KEY_OPTION3 = "option3";
	private static final String KEY_OPTION4 = "option4";
	private static final String KEY_QUANTSSOL = "sol";
	private static final String KEY_SOL = "sol";

	// c language table
	private static final String KEY_CLANGUAGEID = "cid";
	private static final String KEY_CLANGUAGEQUES = "cques";
	private static final String KEY_CCAT = "ccat";
	private static final String KEY_CSOL = "sol";

	// cpp language table
	private static final String KEY_CPPLANGUAGEID = "cppid";
	private static final String KEY_CPPLANGUAGEQUES = "cppques";
	private static final String KEY_CPPCAT = "cppcat";
	private static final String KEY_CPPSOL = "sol";

	// java language table
	private static final String KEY_JAVALANGUAGEID = "javaid";
	private static final String KEY_JAVALANGUAGEQUES = "javaques";
	private static final String KEY_JAVACAT = "javacat";
	private static final String KEY_JAVASOL = "sol";

	// html language table
	private static final String KEY_HTMLLANGUAGEID = "hmtlid";
	private static final String KEY_HTMLLANGUAGEQUES = "htmlques";
	// private static final String KEY_HTMLCAT = "htmlcat";
	private static final String KEY_HTMLSOL = "sol";

	// verbal and logicallanguage table
	private static final String KEY_VLID = "vlid";
	private static final String KEY_VLQUES = "vlques";
	private static final String KEY_VLCAT = "vlcat";
	private static final String KEY_VLSOL = "sol";

	// os table
	private static final String KEY_OSID = "osid";
	private static final String KEY_OSQUES = "osques";
	// private static final String KEY_OSCAT = "oscat";
	private static final String KEY_OSSOL = "sol";

	// dbms table
	private static final String KEY_DBMSID = "dbmsid";
	private static final String KEY_DBMSQUES = "dbmsques";
	// private static final String KEY_OSCAT = "oscat";
	private static final String KEY_DBMSSOL = "sol";

	// dSA table
	private static final String KEY_DSAID = "dsaid";
	private static final String KEY_DSAQUES = "dsaques";
	// private static final String KEY_OSCAT = "oscat";
	private static final String KEY_DSASOL = "sol";

	// tutorial table
	private static final String KEY_TUTID = "tutorialid";
	private static final String KEY_TUTQUES = "tutorialques";
	private static final String KEY_TUTCAT = "tutorialcat";

	// fav table
	private static final String KEY_FAVOURITEID = "favouritelid";
	private static final String KEY_FAVOURITEQUES = "favouriteques";
	// private static final String KEY_HTMLCAT = "htmlcat";
	private static final String KEY_FAVOURITESOL = "sol";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_QUANTS_TABLE = "CREATE TABLE " + TABLE_QUANTS + "("
				+ KEY_QUANTSID + " INTEGER PRIMARY KEY," + KEY_QUANTSQUES
				+ " TEXT," + KEY_QUANTSCAT + " TEXT," + KEY_OPTION1 + " TEXT,"
				+ KEY_OPTION2 + " TEXT," + KEY_OPTION3 + " TEXT," + KEY_OPTION4
				+ " TEXT," + KEY_QUANTSSOL + " TEXT" + ")";

		String CREATE_CLANGUAGE_TABLE = "CREATE TABLE " + TABLE_CLANGUAGE + "("
				+ KEY_CLANGUAGEID + " INTEGER PRIMARY KEY," + KEY_CLANGUAGEQUES
				+ " TEXT," + KEY_CCAT + " TEXT," + KEY_OPTION1 + " TEXT,"
				+ KEY_OPTION2 + " TEXT," + KEY_OPTION3 + " TEXT," + KEY_OPTION4
				+ " TEXT," + KEY_CSOL + " TEXT" + ")";

		String CREATE_CPPLANGUAGE_TABLE = "CREATE TABLE " + TABLE_CPPLANGUAGE
				+ "(" + KEY_CPPLANGUAGEID + " INTEGER PRIMARY KEY,"
				+ KEY_CPPLANGUAGEQUES + " TEXT," + KEY_CPPCAT + " TEXT,"
				+ KEY_OPTION1 + " TEXT," + KEY_OPTION2 + " TEXT," + KEY_OPTION3
				+ " TEXT," + KEY_OPTION4 + " TEXT," + KEY_CPPSOL + " TEXT"
				+ ")";

		String CREATE_JAVALANGUAGE_TABLE = "CREATE TABLE " + TABLE_JAVALANGUAGE
				+ "(" + KEY_JAVALANGUAGEID + " INTEGER PRIMARY KEY,"
				+ KEY_JAVALANGUAGEQUES + " TEXT," + KEY_JAVACAT + " TEXT,"
				+ KEY_OPTION1 + " TEXT," + KEY_OPTION2 + " TEXT," + KEY_OPTION3
				+ " TEXT," + KEY_OPTION4 + " TEXT," + KEY_JAVASOL + " TEXT"
				+ ")";
		String CREATE_HTMLLANGUAGE_TABLE = "CREATE TABLE " + TABLE_HTMLLANGUAGE
				+ "(" + KEY_HTMLLANGUAGEID + " INTEGER PRIMARY KEY,"
				+ KEY_HTMLLANGUAGEQUES + " TEXT," + KEY_OPTION1 + " TEXT,"
				+ KEY_OPTION2 + " TEXT," + KEY_OPTION3 + " TEXT," + KEY_OPTION4
				+ " TEXT," + KEY_HTMLSOL + " TEXT" + ")";

		String CREATE_VL_TABLE = "CREATE TABLE " + TABLE_VL + "(" + KEY_VLID
				+ " INTEGER PRIMARY KEY," + KEY_VLQUES + " TEXT," + KEY_VLCAT
				+ " TEXT," + KEY_OPTION1 + " TEXT," + KEY_OPTION2 + " TEXT,"
				+ KEY_OPTION3 + " TEXT," + KEY_OPTION4 + " TEXT," + KEY_VLSOL
				+ " TEXT" + ")";
		String CREATE_OS_TABLE = "CREATE TABLE " + TABLE_OS + "(" + KEY_OSID
				+ " INTEGER PRIMARY KEY," + KEY_OSQUES + " TEXT," + KEY_OPTION1
				+ " TEXT," + KEY_OPTION2 + " TEXT," + KEY_OPTION3 + " TEXT,"
				+ KEY_OPTION4 + " TEXT," + KEY_OSSOL + " TEXT" + ")";

		String CREATE_DBMS_TABLE = "CREATE TABLE " + TABLE_DBMS + "("
				+ KEY_DBMSID + " INTEGER PRIMARY KEY," + KEY_DBMSQUES
				+ " TEXT," + KEY_OPTION1 + " TEXT," + KEY_OPTION2 + " TEXT,"
				+ KEY_OPTION3 + " TEXT," + KEY_OPTION4 + " TEXT," + KEY_DBMSSOL
				+ " TEXT" + ")";

		String CREATE_DSA_TABLE = "CREATE TABLE " + TABLE_DSA + "(" + KEY_DSAID
				+ " INTEGER PRIMARY KEY," + KEY_DSAQUES + " TEXT,"
				+ KEY_OPTION1 + " TEXT," + KEY_OPTION2 + " TEXT," + KEY_OPTION3
				+ " TEXT," + KEY_OPTION4 + " TEXT," + KEY_DSASOL + " TEXT"
				+ ")";

		String CREATE_TUTORIAL_TABLE = "CREATE TABLE " + TABLE_TUTORIAL + "("
				+ KEY_TUTID + " INTEGER PRIMARY KEY," + KEY_TUTCAT + " TEXT,"
				+ KEY_TUTQUES + " TEXT" + ")";

		String CREATE_FAVOURITE_TABLE = "CREATE TABLE " + TABLE_FAVOURITE + "("
				+ KEY_FAVOURITEID + " INTEGER PRIMARY KEY," + KEY_FAVOURITEQUES
				+ " TEXT," + KEY_OPTION1 + " TEXT," + KEY_OPTION2 + " TEXT,"
				+ KEY_OPTION3 + " TEXT," + KEY_OPTION4 + " TEXT,"
				+ KEY_FAVOURITESOL + " TEXT" + ")";

		String CREATE_SBTABLE_TABLE = "CREATE TABLE " + TABLE_SBTABLE + "("
				+ KEY_SBID + " INTEGER PRIMARY KEY," + KEY_SBSECTION + " TEXT,"
				+ KEY_SBSUBSECTION + " TEXT," + KEY_SBDATETIME + " TEXT,"
				+ KEY_SBSCORE + " TEXT," + KEY_SBTT + " TEXT" + ")";

		String CREATE_PUZZLE_TABLE = "CREATE TABLE " + TABLE_PUZZLETABLE + "("
				+ KEY_PUZZLEID + " INTEGER PRIMARY KEY," + KEY_PUZZLEQUES
				+ " TEXT," + KEY_PUZZLESOL + " TEXT" + ")";

		db.execSQL(CREATE_QUANTS_TABLE);
		db.execSQL(CREATE_CLANGUAGE_TABLE);
		db.execSQL(CREATE_CPPLANGUAGE_TABLE);
		db.execSQL(CREATE_JAVALANGUAGE_TABLE);
		db.execSQL(CREATE_HTMLLANGUAGE_TABLE);
		db.execSQL(CREATE_VL_TABLE);
		db.execSQL(CREATE_OS_TABLE);
		db.execSQL(CREATE_DBMS_TABLE);
		db.execSQL(CREATE_DSA_TABLE);
		db.execSQL(CREATE_TUTORIAL_TABLE);
		db.execSQL(CREATE_FAVOURITE_TABLE);
		db.execSQL(CREATE_SBTABLE_TABLE);
		db.execSQL(CREATE_PUZZLE_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUANTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLANGUAGE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CPPLANGUAGE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_JAVALANGUAGE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_HTMLLANGUAGE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_VL);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DBMS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DSA);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TUTORIAL);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVOURITE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SBTABLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PUZZLETABLE);
		// Create tables again
		onCreate(db);

	}

	/**
	 * All CRUD(Create, Read, Update, Delete) Operations
	 */

	// Adding new PUZZLE TABLE(SBTABLE)
	void addPuzzle(PuzzleTable q) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_PUZZLEQUES, q.getQues()); // Contact Name
		// values.put(KEY_OSCAT, quants.getCat());
		values.put(KEY_PUZZLESOL, q.getSol());

		// Contact Phone

		// Inserting Row

		db.insert(TABLE_PUZZLETABLE, null, values);

		db.close(); // Closing database connection
	}

	// Adding new SCORE TABLE(SBTABLE)
	void addSbtable(sbtable q) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SBSECTION, q.getSection()); // Contact Name
		// values.put(KEY_OSCAT, quants.getCat());
		values.put(KEY_SBSUBSECTION, q.getSubsection());
		values.put(KEY_SBDATETIME, q.getDatetime());
		values.put(KEY_SBSCORE, q.getScore());
		values.put(KEY_SBTT, q.gettt());

		// Contact Phone

		// Inserting Row
		db.insert(TABLE_SBTABLE, null, values);

		db.close(); // Closing database connection
	}

	// ADDING FAV

	// Adding new OS
	void addFav(Favourite q) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FAVOURITEQUES, q.getQues()); // Contact Name

		values.put(KEY_OPTION1, q.getOption1());
		values.put(KEY_OPTION2, q.getOption2());
		values.put(KEY_OPTION3, q.getOption3());
		values.put(KEY_OPTION4, q.getOption4());
		values.put(KEY_FAVOURITESOL, q.getSol());
		// Contact Phone

		// Inserting Row
		db.insert(TABLE_FAVOURITE, null, values);

		db.close(); // Closing database connection
	}

	// Adding new TUTORIAL

	// Adding new QuestionTable
	void addQuestions(QuestionTable questTable) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(KEY_OPTION1, questTable.getOption1());
		values.put(KEY_OPTION2, questTable.getOption2());
		values.put(KEY_OPTION3, questTable.getOption3());
		values.put(KEY_OPTION4, questTable.getOption4());

		values.put(questTable.getKeyQuest(), questTable.getQues());
		values.put(questTable.getKeyCat(), questTable.getCat());
		values.put(KEY_SOL, questTable.getSol());

		// Inserting Row
		db.insert(questTable.getTableCat(), null, values);
		db.close(); // Closing database connection
	}

	// Get a Question
	QuestionTable getQuestion(int id, String cat, QuestionTable qt) {
		SQLiteDatabase db = this.getReadableDatabase();
		// String where = "KEY_QUANTSID=? AND KEY_QUANTSCAT=? ";
		Cursor cursor = db.query(
				qt.getTableCat(),
				new String[] { qt.getKeyQuestID(), qt.getKeyQuest(),
						qt.getKeyCat(), KEY_OPTION1, KEY_OPTION2, KEY_OPTION3,
						KEY_OPTION4, KEY_SOL }, qt.getKeyQuestID() + "=?"
						+ " AND " + qt.getKeyCat() + "=" + "'" + cat + "'",
				new String[] { String.valueOf(id) }, null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();

		qt.setID(Integer.parseInt(cursor.getString(0)));
		qt.setQues(cursor.getString(1));
		qt.setCat(cursor.getString(2));
		qt.setOption1(cursor.getString(3));
		qt.setOption2(cursor.getString(4));
		qt.setOption3(cursor.getString(5));
		qt.setOption4(cursor.getString(6));
		qt.setSol(cursor.getString(7));

		db.close();
		return qt;
	}
	
	public ArrayList <PsyAnswer> getSituationAnswers(int situationID)
	{
		ArrayList <PsyAnswer> answers = new ArrayList<PsyAnswer>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query("psyanswers",null, "situationid=?",
				new String[] { String.valueOf(situationID) }, null, null, null, null);
		if (cursor.moveToFirst())
		{
			do{
				PsyAnswer answer;
				int id = Integer.parseInt(cursor.getString(0));
				String answerStr = cursor.getString(1);
				int priority = Integer.parseInt(cursor.getString(2));
				String comment = cursor.getString(3);
				int sitID = Integer.parseInt(cursor.getString(4));
				
				answer = new PsyAnswer(id, priority,answerStr, comment);
				answers.add(answer);
			}while(cursor.moveToNext());
		}
		db.close();
		
		return answers;
	}
	
	public PsyAnswer getPsyAnswer(int psyAnswerID)
	{
		PsyAnswer answer = null;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query("psyanswers",null, "answerid=?",
				new String[] { String.valueOf(psyAnswerID) }, null, null, null, null);
		if (cursor.moveToFirst())
		{
			int id = Integer.parseInt(cursor.getString(0));
			String answerStr = cursor.getString(1);
			int priority = Integer.parseInt(cursor.getString(2));
			String comment = cursor.getString(3);
			int sitID = Integer.parseInt(cursor.getString(4));
			
			answer = new PsyAnswer(id, priority,answerStr, comment);
		}
		db.close();
		
		return answer;
	}
	
	public PsySituation getPsySituation(int psySituationID)
	{
		PsySituation situation = null;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query("situation",null, "situationid=?",
				new String[] { String.valueOf(psySituationID) }, null, null, null, null);
		if (cursor.moveToFirst())
		{
			int id = Integer.parseInt(cursor.getString(0));
			String situationText = cursor.getString(1);
			String competency = cursor.getString(2);
			ArrayList<PsyAnswer> answers = getSituationAnswers(psySituationID);
			situation = new PsySituation(id, situationText, competency, answers);
		}
		db.close();
		
		return situation;
	}
	
	public ArrayList <PsySituation> getScenarioSituations(int scenarioID)
	{
		ArrayList <PsySituation> situations = new ArrayList<PsySituation>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query("situation",null, "scenarioid=?",
				new String[] { String.valueOf(scenarioID) }, null, null, null, null);
		if (cursor.moveToFirst())
		{
			do{
				PsySituation situation;
				int id = Integer.parseInt(cursor.getString(0));
				String situationText = cursor.getString(1);
				String competency = cursor.getString(2);
				//int scenID = Integer.parseInt(cursor.getString(3));
				ArrayList<PsyAnswer> answers = getSituationAnswers(id);
				
				situation = new PsySituation(id, situationText, competency, answers);
				situations.add(situation);
			}while(cursor.moveToNext());
		}
		db.close();
		
		return situations;
	}

	public PsyScenario getPsyScenario(int psyScenarioID)
	{
		PsyScenario scenario = null;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query("scenario",null, "scenarioid=?",
				new String[] { String.valueOf(psyScenarioID) }, null, null, null, null);
		if (cursor.moveToFirst())
		{
			int id = Integer.parseInt(cursor.getString(0));
			String scenarioText = cursor.getString(1);
			
			ArrayList<PsySituation> situations = getScenarioSituations(id);
			scenario = new PsyScenario(id, scenarioText, situations);
		}
		db.close();
		
		return scenario;
	}

	// Get a Question
	QuestionTable getQuestionShort(int id, String cat, QuestionTable qt) {
		SQLiteDatabase db = this.getReadableDatabase();
		// String where = "KEY_QUANTSID=? AND KEY_QUANTSCAT=? ";
		Cursor cursor = db.query(qt.getTableCat(),
				new String[] { qt.getKeyQuestID(), qt.getKeyQuest(),
						KEY_OPTION1, KEY_OPTION2, KEY_OPTION3, KEY_OPTION4,
						KEY_SOL },
				qt.getKeyQuestID() + "=?" + " AND " + qt.getKeyCat() + "="
						+ "'" + cat + "'", new String[] { String.valueOf(id) },
				null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();

		qt.setID(Integer.parseInt(cursor.getString(0)));
		qt.setQues(cursor.getString(1));
		qt.setOption1(cursor.getString(2));
		qt.setOption2(cursor.getString(3));
		qt.setOption3(cursor.getString(4));
		qt.setOption4(cursor.getString(5));
		qt.setSol(cursor.getString(6));

		db.close();
		return qt;
	}

	QuestionTable getQuestion(int id, QuestionTable qt) {
		SQLiteDatabase db = this.getReadableDatabase();
		// String where = "KEY_QUANTSID=? AND KEY_QUANTSCAT=? ";
		Cursor cursor = db.query(
				qt.getTableCat(),
				new String[] { qt.getKeyQuestID(), qt.getKeyQuest(),
						qt.getKeyCat(), KEY_OPTION1, KEY_OPTION2, KEY_OPTION3,
						KEY_OPTION4, KEY_SOL }, qt.getKeyQuestID() + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);

		if (cursor != null)
			cursor.moveToFirst();

		qt.setID(Integer.parseInt(cursor.getString(0)));
		qt.setQues(cursor.getString(1));
		qt.setCat(cursor.getString(2));
		qt.setOption1(cursor.getString(3));
		qt.setOption2(cursor.getString(4));
		qt.setOption3(cursor.getString(5));
		qt.setOption4(cursor.getString(6));
		qt.setSol(cursor.getString(7));

		db.close();
		return qt;
	}

	QuestionTable getQuestionShort(int id, QuestionTable qt) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(qt.getTableCat(),
				new String[] { qt.getKeyQuestID(), qt.getKeyQuest(),
						KEY_OPTION1, KEY_OPTION2, KEY_OPTION3, KEY_OPTION4,
						KEY_SOL }, qt.getKeyQuestID() + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		qt.setID(Integer.parseInt(cursor.getString(0)));
		qt.setQues(cursor.getString(1));
		qt.setOption1(cursor.getString(2));
		qt.setOption2(cursor.getString(3));
		qt.setOption3(cursor.getString(4));
		qt.setOption4(cursor.getString(5));
		qt.setSol(cursor.getString(6));

		// return contact
		return qt;
	}

	public List<QuestionTable> getAllQuestions(String cat, QuestionTable qt) {
		List<QuestionTable> quantsList = new ArrayList<QuestionTable>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + qt.getTableCat() + " where "
				+ qt.getKeyCat() + "=" + "'" + cat + "'";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to li
		if (cursor.moveToFirst()) {
			do {
				QuestionTable quants = new QuestionTable();
				quants.setID(Integer.parseInt(cursor.getString(0)));
				quants.setQues(cursor.getString(1));
				quants.setCat(cursor.getString(2));
				quants.setOption1(cursor.getString(3));
				quants.setOption2(cursor.getString(4));
				quants.setOption3(cursor.getString(5));
				quants.setOption4(cursor.getString(6));
				quants.setSol(cursor.getString(7));
				// Adding contact to list
				quantsList.add(quants);
			} while (cursor.moveToNext());
		}
		// return contact list
		db.close();
		return quantsList;
	}

	public List<QuestionTable> getAllQuestions(QuestionTable qt) {
		List<QuestionTable> quantsList = new ArrayList<QuestionTable>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + qt.getTableCat();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to li
		if (cursor.moveToFirst()) {
			do {
				QuestionTable quants = new QuestionTable();
				quants.setID(Integer.parseInt(cursor.getString(0)));
				quants.setQues(cursor.getString(1));
				quants.setCat(cursor.getString(2));
				quants.setOption1(cursor.getString(3));
				quants.setOption2(cursor.getString(4));
				quants.setOption3(cursor.getString(5));
				quants.setOption4(cursor.getString(6));
				quants.setSol(cursor.getString(7));
				// Adding contact to list
				quantsList.add(quants);
			} while (cursor.moveToNext());
		}
		// return contact list
		db.close();
		return quantsList;
	}

	public List<QuestionTable> getAllQuestionsShort(String cat, QuestionTable qt) {
		List<QuestionTable> quantsList = new ArrayList<QuestionTable>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + qt.getTableCat() + " where "
				+ qt.getKeyCat() + "=" + "'" + cat + "'";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to li
		if (cursor.moveToFirst()) {
			do {
				QuestionTable quants = new QuestionTable();
				quants.setID(Integer.parseInt(cursor.getString(0)));
				quants.setQues(cursor.getString(1));
				quants.setOption1(cursor.getString(2));
				quants.setOption2(cursor.getString(3));
				quants.setOption3(cursor.getString(4));
				quants.setOption4(cursor.getString(5));
				quants.setSol(cursor.getString(6));
				// Adding contact to list
				quantsList.add(quants);
			} while (cursor.moveToNext());
		}
		// return contact list
		db.close();
		return quantsList;
	}

	public List<QuestionTable> getAllQuestionsShort(QuestionTable qt) {
		List<QuestionTable> quantsList = new ArrayList<QuestionTable>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + qt.getTableCat();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to li
		if (cursor.moveToFirst()) {
			do {
				QuestionTable quants = new QuestionTable();
				quants.setID(Integer.parseInt(cursor.getString(0)));
				quants.setQues(cursor.getString(1));
				// quants.setCat(cursor.getString(2));
				quants.setOption1(cursor.getString(2));
				quants.setOption2(cursor.getString(3));
				quants.setOption3(cursor.getString(4));
				quants.setOption4(cursor.getString(5));
				quants.setSol(cursor.getString(6));
				// Adding contact to list
				quantsList.add(quants);
			} while (cursor.moveToNext());
		}
		// return contact list
		db.close();
		return quantsList;
	}

	// Deleting single Question
	public void deleteQuestion(QuestionTable qt) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(qt.getTableCat(), qt.getKeyQuestID() + " = ?",
				new String[] { String.valueOf(qt.getID()) });
		db.close();
	}

	public int getQuestionsCount(QuestionTable qt) {
		String countQuery = "SELECT  * FROM " + qt.getTableCat();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

	public int updateQuestion(QuestionTable qt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(qt.getKeyQuest(), qt.getQues()); // Contact Name
		values.put(qt.getKeyCat(), qt.getCat());
		values.put(KEY_OPTION1, qt.getOption1());
		values.put(KEY_OPTION2, qt.getOption2());
		values.put(KEY_OPTION3, qt.getOption3());
		values.put(KEY_OPTION4, qt.getOption4());
		values.put(KEY_SOL, qt.getSol());

		// updating row
		return db.update(qt.getTableCat(), values, qt.getKeyQuestID() + " = ?",
				new String[] { String.valueOf(qt.getID()) });
	}

	public int updateQuestionShort(QuestionTable qt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(qt.getKeyQuest(), qt.getQues());
		values.put(KEY_OPTION1, qt.getOption1());
		values.put(KEY_OPTION2, qt.getOption2());
		values.put(KEY_OPTION3, qt.getOption3());
		values.put(KEY_OPTION4, qt.getOption4());
		values.put(KEY_SOL, qt.getSol());

		// updating row
		return db.update(qt.getTableCat(), values, qt.getKeyQuestID() + " = ?",
				new String[] { String.valueOf(qt.getID()) });
	}

	// ADDING TO JAVALANG TABLE

	// ADDING TOHTMLLANG TABLE

	// Getting single sbtable score

	sbtable getSbtable(String cat) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_SBTABLE, new String[] { KEY_SBID,
				KEY_SBSECTION, KEY_SBSUBSECTION, KEY_SBDATETIME, KEY_SBSCORE },
				KEY_SBSUBSECTION + "=?", new String[] { String.valueOf(cat) },
				null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		sbtable v = new sbtable(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3),
				cursor.getString(4), cursor.getString(5));
		// return contact
		return v;
	}

	// Getting single puzzle
	PuzzleTable getPuzzle(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_PUZZLETABLE, new String[] {
				KEY_PUZZLEID, KEY_PUZZLEQUES, KEY_PUZZLESOL }, KEY_PUZZLEID
				+ "=?", new String[] { String.valueOf(id) }, null, null, null,
				null);
		if (cursor != null)
			cursor.moveToFirst();

		PuzzleTable v = new PuzzleTable(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2));
		// return contact
		return v;
	}

	// Getting single tuts

	//
	// Getting single fav
	Favourite getFav(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_FAVOURITE, new String[] {
				KEY_FAVOURITEID, KEY_FAVOURITEQUES, KEY_OPTION1, KEY_OPTION2,
				KEY_OPTION3, KEY_OPTION4, KEY_FAVOURITESOL }, KEY_FAVOURITEID
				+ "=?", new String[] { String.valueOf(id) }, null, null, null,
				null);
		if (cursor != null)
			cursor.moveToFirst();

		Favourite v = new Favourite(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2), cursor.getString(3),
				cursor.getString(4), cursor.getString(5), cursor.getString(6));
		// return contact
		return v;
	}

	// Getting All sbtable
	public List<sbtable> getAllsbtable(String cat) {
		List<sbtable> quantsList = new ArrayList<sbtable>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SBTABLE + " where "
				+ KEY_SBSUBSECTION + "=" + "'" + cat + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to li
		if (cursor.moveToFirst()) {
			do {

				sbtable quants = new sbtable();
				quants.setID(Integer.parseInt(cursor.getString(0)));
				quants.setSection(cursor.getString(1));
				quants.setSubsection(cursor.getString(2));
				quants.setdatetime(cursor.getString(3));
				quants.setScore(cursor.getString(4));
				quants.settt(cursor.getString(5));

				// Adding contact to list
				quantsList.add(quants);

			} while (cursor.moveToNext());
		}

		// return contact list
		db.close();
		return quantsList;
	}

	// Getting All Tuts

	// Getting All puzzle
	public List<PuzzleTable> getAllPuzzle() {
		List<PuzzleTable> VList = new ArrayList<PuzzleTable>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_PUZZLETABLE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to li
		if (cursor.moveToFirst()) {
			do {

				PuzzleTable v = new PuzzleTable();
				v.setID(Integer.parseInt(cursor.getString(0)));
				v.setQues(cursor.getString(1));
				// v.setCat(cursor.getString(2));

				v.setSol(cursor.getString(2));
				// Adding contact to list
				VList.add(v);
			} while (cursor.moveToNext());
		}

		// return contact list
		return VList;
	}

	// Getting All Fav
	public List<Favourite> getAllFav() {
		List<Favourite> VList = new ArrayList<Favourite>();
		// Select All Query

		String selectQuery = "SELECT  * FROM " + TABLE_FAVOURITE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to li
		if (cursor.moveToFirst()) {
			do {
				Favourite v = new Favourite();
				v.setID(Integer.parseInt(cursor.getString(0)));
				v.setQues(cursor.getString(1));

				v.setOption1(cursor.getString(2));
				v.setOption2(cursor.getString(3));
				v.setOption3(cursor.getString(4));
				v.setOption4(cursor.getString(5));
				v.setSol(cursor.getString(6));
				// Adding contact to list
				VList.add(v);
			} while (cursor.moveToNext());
		}

		// return contact list
		return VList;
	}

	// Deleting single fav
	public void deletefav(Favourite v) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_FAVOURITE, KEY_FAVOURITEID + " = ?",
				new String[] { String.valueOf(v.getID()) });
		db.close();
	}

	public void deletesb(sbtable v) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_SBTABLE, KEY_SBID + " = ?",
				new String[] { String.valueOf(v.getID()) });
		db.close();
	}

	// Getting DSAcount
	public int getFavCount() {

		String countQuery = "SELECT  * FROM " + TABLE_FAVOURITE;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

	public ArrayList<sbtable> returnRS() {
		ArrayList<sbtable> currency = new ArrayList<sbtable>();
		// updateQuestThirteen();
		Log.e("", "check");
		String query = "SELECT *  FROM quants";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		Log.e("", "check2" + cursor.getCount());
		if (cursor.moveToFirst()) {
			Log.e("", "check3");
			do {
				sbtable quants = new sbtable();
				quants.setID(Integer.parseInt(cursor.getString(0)));
				quants.setSection(cursor.getString(1));
				currency.add(quants);

			} while (cursor.moveToNext());
		}
		return currency;
	}

	public int updateQuestThirteen() {
		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues cv = new ContentValues();
		String str = "quantsid=13";
		cv.put("quantsques",
				"The price of 10 chairs is equal to that of 4 tables. The price of 15 chairs and 2 tables together is $Rs.$ 4000. The total priceod 12 chairs and 3 tables is: ");
		return db.update("quants", cv, str, null);
	}

	public int updateQuest(String quest, int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues cv = new ContentValues();
		String str = "quantsid=" + id;
		cv.put("quantsques", quest);
		return db.update("quants", cv, str, null);

	}

	public ArrayList<QuestionTable> returnAns() {
		ArrayList<QuestionTable> currencyAns = new ArrayList<QuestionTable>();
		// updateQuestThirteen();
		String query = "SELECT *  FROM " + TABLE_QUANTS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {
				QuestionTable quants = new QuestionTable();
				quants.setID(Integer.parseInt(cursor.getString(0)));
				quants.setOption1(cursor.getString(3));
				quants.setOption2(cursor.getString(4));
				quants.setOption3(cursor.getString(5));
				quants.setOption4(cursor.getString(6));
				currencyAns.add(quants);

			} while (cursor.moveToNext());
		}
		return currencyAns;

	}

	public int updateAns(String answer, int id, String answerNumber) {
		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues cv = new ContentValues();
		String option = "option" + answerNumber;
		String str = "quantsid=" + id;
		cv.put(option, answer);
		return db.update("quants", cv, str, null);

	}
	
	
}