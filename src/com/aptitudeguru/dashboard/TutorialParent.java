package com.aptitudeguru.dashboard;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.widget.Button;
import android.widget.Toast;

public class TutorialParent extends AptitudeBaseActivity{
	
	public void createButtonsListeners(int count,int PDFcount, String[] PDFs, ArrayList<Button> buttons){
		
		for (int k = count; k < buttons.size(); k++){
			buttons.get(k).setTag(PDFs[k-PDFcount]);
			buttons.get(k).setOnClickListener(new View.OnClickListener() {
			
				@Override
				public void onClick(View view) {
					String pdf = (String) view.getTag();

					AssetManager assetManager = getAssets();
					InputStream in = null;
					OutputStream out = null;
					File file = new File(getFilesDir(), pdf);
					try {
						in = assetManager.open(pdf);
						out = openFileOutput(file.getName(),
								Context.MODE_WORLD_READABLE);

						copyFile(in, out);
						in.close();
						in = null;
						out.flush();
						out.close();
						out = null;
					} catch (Exception e) {
						Log.e("tag", e.getMessage());
					}
					try
					{
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(
							Uri.parse("file://" + getFilesDir() + "/"+ pdf),
							"application/pdf");

					startActivity(intent);
					}
					catch(Exception e)
					{
						Toast.makeText(TutorialParent.this, "No Application available to view pdf", Toast.LENGTH_LONG).show(); 
					}
				}

				private void copyFile(InputStream in, OutputStream out)
						throws IOException {
					byte[] buffer = new byte[1024];
					int read;
					while ((read = in.read(buffer)) != -1) {
						out.write(buffer, 0, read);
					}
		

				}
			});
		}
		
	}
	
	
	

}
