package com.aptitudeguru.dashboard;

import java.util.Currency;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;


public class PhoneLocale {

	private Locale loc;
	private String currentCountry;
	Currency currency; 

	public PhoneLocale()
	{
		loc = Resources.getSystem().getConfiguration().locale;
		currency = Currency.getInstance(loc);
		currentCountry = loc.getCountry();
	}
	
	public PhoneLocale(Locale loc)
	{
		this.loc = loc;
		currency = Currency.getInstance(loc);
		currentCountry = loc.getCountry();
	}

	public String getCurrentCountry(){
		System.out.println(currentCountry); // debug message to console
		return currentCountry;
	}

	public Locale getLocale(){
		System.out.println(" Locale "+ loc );
		return loc;
	}
	// Returns "US Dollar" for US

	@SuppressLint("NewApi")
	public String getCurrencyName(){
		String currencyName = currency.getCurrencyCode();
		if(android.os.Build.VERSION.SDK_INT > 18)
		{
			currencyName = currency.getDisplayName();
		}
		else
		{
			if (currentCountry.equals("US")) {
				currencyName = "dollar";
			}
			else if (currentCountry.equals("GB")) {
				currencyName = "pound";
			}
			else if (currentCountry.equals("FR"))
			{
				currencyName = "euro";
			}
		}
		// This doesn't work properly on some emulators and on android v<5
		//Currency currency = java.util.Currency.getInstance(getLocale());
		//String currencyName = currency.getDisplayName(getLocale());
		//System.out.println("CURRENCY " + currencyName);
		return currencyName;	
	}

	public String getCurrencyCode(){
		String currencyCode = currency.getCurrencyCode();
		return currencyCode;
	}
	
	public String getSymbol()
	{
		return "\\" + currency.getSymbol();
	}

	public String getUnits(){
		String units;
		if (currentCountry.equals("US") || currentCountry.equals("GB")){
			units = "imperial";
		}
		else{
			units = "metric";
		}
		return units;
	}
}