package com.aptitudeguru.dashboard;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.widget.TextView;

public class MyCountDownTimer  extends CountDownTimer{

	private int seconds;
	private int minutes;
	private long timeTillFinish;
	TextView timeDisplay;
	Context context ;
	boolean alertRan = false;
	boolean show = true;
	public MyCountDownTimer(long startTime,long interval, TextView timeDisplay,Context context) {
		super(startTime, interval);
		timeTillFinish=startTime;
	 	seconds = (int) (timeTillFinish / 1000) % 60;
		minutes = (int) ((timeTillFinish / (1000 * 60)) % 60);
		this.timeDisplay=timeDisplay;
		this.context=context;
	}
	
	public MyCountDownTimer(long startTime,long interval, TextView timeDisplay,Context context, boolean dev) {
		super(startTime, interval);
		timeTillFinish=startTime;
	 	seconds = (int) (timeTillFinish / 1000) % 60;
		minutes = (int) ((timeTillFinish / (1000 * 60)) % 60);
		this.timeDisplay=timeDisplay;
		this.context=context;
		show = false;
	}
	
	public void onTick(long millisUntilFinished) {
   	 
		timeTillFinish = millisUntilFinished;
	 	seconds = (int) (timeTillFinish / 1000) % 60;
		minutes = (int) ((timeTillFinish / (1000 * 60)) % 60);
		
		if(seconds==0 && minutes==1)
		{
			Alert(timeTillFinish);	
		}
		timeDisplay.setText("Time left: " + minutes + ":" + String.format("%02d", seconds));
 }

 public void onFinish() {
	 	timeDisplay.setText("Time's up!");
		AlertDialog alertDialog = new AlertDialog.Builder(context)
				.create();
	 	TextView title = new TextView(context);
		title.setText("Aptitude App");
		title.setBackgroundColor(Color.DKGRAY);
		title.setPadding(10, 10, 10, 10);
		title.setGravity(Gravity.CENTER);
		title.setTextColor(Color.WHITE);
		title.setTextSize(20);
		alertDialog.setCustomTitle(title);
		alertDialog.setMessage("TIME'S UP");
 }
 
 public int getSeconds()
 {
	 return seconds;
 }
 
 public int getMinutes()
 {
	 return minutes;
 }
 public long returnMillies()
 {
	 return timeTillFinish;
 }
 

 
 public boolean getAlertRan()
 {
	 return alertRan;
 }
 
	private void Alert(final long current)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		TextView title = new TextView(context);
		title.setText("Aptitude App");
		title.setBackgroundColor(Color.DKGRAY);
		title.setPadding(10, 10, 10, 10);
		title.setGravity(Gravity.CENTER);
		title.setTextColor(Color.WHITE);
		title.setTextSize(20);
		alertDialogBuilder.setCustomTitle(title);

		// set dialog message
		alertDialogBuilder
				.setMessage("One minute remaining")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			           public void onClick(DialogInterface dialog, int id) {
			        
			           }
			       });
		// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				if (show)
				{
					alertDialog.show();
				}
				alertRan = true;
	}

	public TextView getTimeDisplay() {
		return timeDisplay;
	}
}

