package com.aptitudeguru.dashboard;

public class JavaTable extends QuestionTable{

	public JavaTable()
	{
		super();
		tableCat = "java";
		key_questID = "javaid";
		key_quest = "javaques";
		key_cat = "javacat";
	}
}
