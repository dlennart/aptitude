package com.aptitudeguru.dashboard;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidhive.dashboard.R;

public class TestPage extends Activity implements OnClickListener

{	
	TextView questionTextDisplay, questionTrack, timeDisplay;
	RadioButton optionBtn1, optionBtn2, optionBtn3, optionBtn4;
	static int min = 0, sec = 0;
	int buttonSelected[] = new int[40];
	String category = "";
	
	int currentQuestionNumber = 0; //Currently runs for 0 to 19
	int questionIDs[] = new int[40];
	int allQuestionsFromDatabase[] = new int[40];
	int allAnswersFromDatabase[] = new int[40];
	int solutions[] = new int[40];
	int gotoQuestion[] = new int[20];
	Random random = new Random();
	final static int GOTO_REQUEST_CODE = 1;
	final Context context = this;
	String time;

	Button btn_next;
	Button btn_prev;

	DatabaseHandler db = new DatabaseHandler(this);
	private CountDownTimer countDownTimer;

	private boolean timerHasStarted = false;
	private static final long STARTTIME = 60 * 20 * 1000;
	private static final long INTERVAL = 1 * 1000;
	private long currentTime = 60 * 20 * 1000;
	private long timeTillFinish = 0;

	boolean testStarted = false;

	// create button instances

	@Override
	protected void onPause() {
		// Always call the superclass method first
		super.onPause();

		countDownTimer.cancel();

		// Activity being restarted from stopped state
	}

	@Override
	protected void onResume() {
		// Always call the superclass method first
		super.onResume();
		countDownTimer.cancel();

		if (testStarted) {
			timerHasStarted = false;

			if (!timerHasStarted) {
				// countDownTimer.start();
				countDownTimer = new MyCountDownTimer(currentTime, INTERVAL);
				timerHasStarted = true;

				countDownTimer.start();

				// startB.setText("STOP");
			} else {
				countDownTimer.cancel();
				timerHasStarted = false;
				// startB.setText("RESTART");
			}

			// Activity being restarted from stopped state
		}
	}

	@Override
	public void onBackPressed() {
		Toast.makeText(getApplicationContext(), "You Cannot Exit",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		String extraData = data.getStringExtra("ComingFrom");
		int questionIndex = Integer.parseInt(extraData);

		int questionID = questionIDs[questionIndex];
		currentQuestionNumber = questionIndex;

		if (currentQuestionNumber == 0) {
			btn_prev.setEnabled(false);

			btn_prev.setVisibility(View.INVISIBLE);
		} else {
			btn_next.setEnabled(true);
			btn_prev.setEnabled(true);
			btn_next.setVisibility(View.VISIBLE);
			btn_prev.setVisibility(View.VISIBLE);
		}
		if (currentQuestionNumber == 19) {
			btn_next.setEnabled(false);
			btn_next.setVisibility(View.INVISIBLE);

		}

		else {
			btn_prev.setEnabled(true);
			btn_next.setEnabled(true);
			btn_next.setVisibility(View.VISIBLE);
			btn_prev.setVisibility(View.VISIBLE);
		}
		optionBtn1.setChecked(false);
		optionBtn2.setChecked(false);
		optionBtn3.setChecked(false);
		optionBtn4.setChecked(false);

		RadioGroup radiogroup = (RadioGroup) findViewById(R.id.options);
		radiogroup.clearCheck();
		int check = buttonSelected[currentQuestionNumber];
		if (check == 1)
			optionBtn1.setChecked(true);
		else if (check == 2)
			optionBtn2.setChecked(true);
		else if (check == 3)
			optionBtn3.setChecked(true);
		else if (check == 4)
			optionBtn4.setChecked(true);
		else {
		}
		
		fetchQuestion(questionID, category);
		questionTrack.setText("   " + (questionIndex + 1) + "/20");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test);

		Bundle bundle = getIntent().getExtras();
		category = bundle.getString("cat");

		optionBtn1 = (RadioButton) findViewById(R.id.option1);
		optionBtn2 = (RadioButton) findViewById(R.id.option2);
		optionBtn3 = (RadioButton) findViewById(R.id.option3);
		optionBtn4 = (RadioButton) findViewById(R.id.option4);

		timeDisplay = (TextView) this.findViewById(R.id.timer);
		countDownTimer = new MyCountDownTimer(STARTTIME, INTERVAL);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		TextView title = new TextView(context);
		title.setText("Aptitude App");
		title.setBackgroundColor(Color.DKGRAY);
		title.setPadding(10, 10, 10, 10);
		title.setGravity(Gravity.CENTER);
		title.setTextColor(Color.WHITE);
		title.setTextSize(20);
		alertDialogBuilder.setCustomTitle(title);

		// set title
		// alertDialogBuilder.setTitle("Apptitude App");

		// set dialog message
		alertDialogBuilder
				.setMessage("Start Test?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								// if this button is clicked, close
								// current activity

								if (!timerHasStarted) {
									countDownTimer.start();
									testStarted = true;
									timerHasStarted = true;
									// startB.setText("STOP");
								} else {
									countDownTimer.cancel();
									timerHasStarted = false;
									// startB.setText("RESTART");
								}

								// next
								dialog.cancel();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing

						TestPage.this.finish();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();

		// timer code

		// startB = (Button) this.findViewById(R.id.button);
		// startB.setOnClickListener( this);

		// text.setText(text.getText() + String.valueOf(startTime / 1000));
		

		/**
		 * Creating all buttons instances
		 * */
		// Dashboard News feed button
		Button btn_home = (Button) findViewById(R.id.btn_home);

		// Dashboard Friends button
		Button btn_fav = (Button) findViewById(R.id.btn_fav);

		// Dashboard Messages button
		Button btn_hint = (Button) findViewById(R.id.btn_hint);

		// Dashboard Places button
		Button btn_goto = (Button) findViewById(R.id.btn_goto);

		// Dashboard Events button
		Button btn_help = (Button) findViewById(R.id.btn_help);

		btn_help.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				Intent i = new Intent(getApplicationContext(), help1.class);

				startActivity(i);
			}
		});

		Button btn_finish = (Button) findViewById(R.id.btn_finish);

		// PAUSE TEST CODE
		Button btn_pause = (Button) findViewById(R.id.btn_pause);
		btn_pause.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				Intent i = new Intent(getApplicationContext(), TestPause.class);
				i.putExtra("cat", category);
				startActivity(i);
			}
		});

		/**
		 * Handling all button click events
		 * */
		RadioGroup radiogroup = (RadioGroup) findViewById(R.id.options);
		radiogroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// RadioButton r1=(RadioButton)findViewById(R.id.option1);
				if (optionBtn1.isChecked()) {
					buttonSelected[currentQuestionNumber] = 1;
					//ans[currentQuestionNumber] = 1;
					gotoQuestion[currentQuestionNumber] = 1;
					// //String j=1+"";
					// btn_cal+j.setBackgroundColor(Color.RED);

				} else if (optionBtn2.isChecked()) {
					buttonSelected[currentQuestionNumber] = 2;
					//ans[currentQuestionNumber] = 2;
					gotoQuestion[currentQuestionNumber] = 1;
				} else if (optionBtn3.isChecked()) {
					buttonSelected[currentQuestionNumber] = 3;
					//ans[currentQuestionNumber] = 3;
					gotoQuestion[currentQuestionNumber] = 1;
				} else if (optionBtn4.isChecked()) {
					buttonSelected[currentQuestionNumber] = 4;
					//ans[currentQuestionNumber] = 4;
					gotoQuestion[currentQuestionNumber] = 1;
				} else {
				}

			}

		});

		btn_finish.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);

				TextView title = new TextView(context);
				title.setText("Aptitude App");
				title.setBackgroundColor(Color.DKGRAY);
				title.setPadding(10, 10, 10, 10);
				title.setGravity(Gravity.CENTER);
				title.setTextColor(Color.WHITE);
				title.setTextSize(20);
				alertDialogBuilder.setCustomTitle(title);

				// set title
				// alertDialogBuilder.setTitle();

				// set dialog message
				alertDialogBuilder
						.setMessage("Click yes to exit!")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, close
										// current activity
										Intent i = new Intent(
												getApplicationContext(),
												ShowScore.class);
										time = timeDisplay.getText() + "";

										sec = sec + 40;
										String timetaken = min + "." + sec + "";

										double timetak = Float
												.parseFloat(timetaken);

										double tt = 20.00 - timetak;

										DecimalFormat df = new DecimalFormat(
												"00.00");
										String j = df.format(tt);

										i.putExtra("score", buttonSelected);
										i.putExtra("givenans", solutions);
										i.putExtra("allid", questionIDs);
										i.putExtra("tt", j);
										i.putExtra("category", category);

										startActivity(i);
										TestPage.this.finish();
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										// if this button is clicked, just close
										// the dialog box and do nothing
										dialog.cancel();

									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it

				alertDialog.show();

			}
		});

		// Listening Friends button click
		btn_fav.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				// Intent i = new Intent(getApplicationContext(),
				// FriendsActivity.class);
				// startActivity(i);
				
				addToFav();

				Toast.makeText(getApplicationContext(), "Added To Favourite",
						Toast.LENGTH_SHORT).show();

				// show it

			}
		});

		// Listening Messages button click
		// Diff 4 - Missing from other class
		btn_hint.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				Intent i = new Intent(getApplicationContext(), Hint.class);
				i.putExtra("cat", category);
				startActivity(i);
			}
		});

		// Listening to Places button click
		btn_goto.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				Intent i = new Intent(getApplicationContext(), Calender.class);
				i.putExtra("gotoclick", gotoQuestion);
				i.putExtra("click", currentQuestionNumber);
				startActivityForResult(i, GOTO_REQUEST_CODE);
			}
		});

		// Listening to Events button click

		fetchAllQuestions(category);

		int count = random.nextInt(3);
		count = count + 1;

		questionTextDisplay = (TextView) findViewById(R.id.textView1);
		questionTrack = (TextView) findViewById(R.id.questrack);

		btn_next = (Button) findViewById(R.id.btn_next);
		btn_prev = (Button) findViewById(R.id.btn_prev);

		int i = 0;
		fetchQuestion(allQuestionsFromDatabase[count], category);
		questionIDs[i++] = allQuestionsFromDatabase[count];
		solutions[0] = allAnswersFromDatabase[count];
		questionTrack.setText("   " + "1/20");
		
		btn_next.setVisibility(View.VISIBLE);
		btn_prev.setVisibility(View.INVISIBLE);
		for (int x = 1; x < 20; x++) {
			int k = (count + 1);
			count = k;
			questionIDs[i] = allQuestionsFromDatabase[k];
			solutions[i] = allAnswersFromDatabase[k];
			i++;
		}

		btn_next.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				if (currentQuestionNumber == (19)) {
					btn_next.setEnabled(false);
					btn_next.setVisibility(View.INVISIBLE);

				} else {

					btn_next.setEnabled(true);
					btn_prev.setEnabled(true);
					btn_next.setVisibility(View.VISIBLE);
					btn_prev.setVisibility(View.VISIBLE);
					optionBtn1.setChecked(false);
					optionBtn2.setChecked(false);
					optionBtn3.setChecked(false);
					optionBtn4.setChecked(false);
					RadioGroup radiogroup = (RadioGroup) findViewById(R.id.options);
					radiogroup.clearCheck();

					// radiogroup.setosetOnCheckedChangeListener(this);

					currentQuestionNumber = currentQuestionNumber + 1;

					int val = questionIDs[currentQuestionNumber];
					int check = buttonSelected[currentQuestionNumber];
					if (check == 1)
						optionBtn1.setChecked(true);
					else if (check == 2)
						optionBtn2.setChecked(true);
					else if (check == 3)
						optionBtn3.setChecked(true);
					else if (check == 4)
						optionBtn4.setChecked(true);
					else {
					}
					questionTrack.setText("   " + (currentQuestionNumber + 1) + "/20");

					fetchQuestion(val, category);
					// i=i+1;
					

					
					// t1.setText();
					
					// radiogroup.setOnCheckedChangeListener(this);

				}

			}
		});
		// prev
		btn_prev.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				if (currentQuestionNumber == 0) {
					btn_prev.setEnabled(false);
					btn_prev.setVisibility(View.INVISIBLE);

				} else {

					btn_prev.setEnabled(true);

					btn_next.setEnabled(true);
					btn_next.setVisibility(View.VISIBLE);
					btn_prev.setVisibility(View.VISIBLE);
					optionBtn1.setChecked(false);
					optionBtn2.setChecked(false);
					optionBtn3.setChecked(false);
					optionBtn4.setChecked(false);
					RadioGroup radiogroup = (RadioGroup) findViewById(R.id.options);
					radiogroup.clearCheck();

					currentQuestionNumber = currentQuestionNumber - 1;
					int val = questionIDs[currentQuestionNumber];
					int check = buttonSelected[currentQuestionNumber];
					if (check == 1)
						optionBtn1.setChecked(true);
					else if (check == 2)
						optionBtn2.setChecked(true);
					else if (check == 3)
						optionBtn3.setChecked(true);
					else if (check == 4)
						optionBtn4.setChecked(true);
					else {
					}


					fetchQuestion(val, category);
					questionTrack.setText("   " + (currentQuestionNumber + 1) + "/20");

				}
			}
		});

	}
	

	protected void fetchAllQuestions (String category) {
		int g = 0;
		List<QuestionTable> questionList = db.getAllQuestions(category, new QuantsTable());
		for (QuestionTable cn : questionList) {
			if (g == 38)
				break;
			else {
				int currentID = cn.getID();
				String sol1 = cn.getSol();
				int sol = 0;
				if (sol1.equalsIgnoreCase("a"))
					sol = 1;
				else if (sol1.equalsIgnoreCase("b"))
					sol = 2;
				else if (sol1.equalsIgnoreCase("c"))
					sol = 3;
				else
					sol = 4;
				// int j=r.nextInt(2);
				// int k=(count+j+1);
				// count=k;
				allQuestionsFromDatabase[g] = currentID;
				allAnswersFromDatabase[g] = sol;
				g++;
			}
		}
	}

	protected void fetchQuestion(int questionID, String category) {
		QuestionTable q = db.getQuestion(questionID, category, new QuantsTable());
		String newQuestion = q.getQues();

		questionTextDisplay.setText(newQuestion);
		
		String opt1 = q.getOption1();
		String opt2 = q.getOption2();
		String opt3 = q.getOption3();
		String opt4 = q.getOption4();
		
		optionBtn1.setText(opt1);
		optionBtn2.setText(opt2);
		optionBtn3.setText(opt3);
		optionBtn4.setText(opt4);
	}


	protected void addToFav()
	{
		int val = questionIDs[currentQuestionNumber];
		QuestionTable q = db.getQuestion(val, category, new QuantsTable());
		String ques = q.getQuesOrig();
		String op1 = q.getOption1Orig();
		String op2 = q.getOption2Orig();
		String op3 = q.getOption3Orig();
		String op4 = q.getOption4Orig();
		String sol = q.getSolOrig();
		db.addFav(new Favourite(ques, op1, op2, op3, op4, sol));
	}

	// timer
	@Override
	public void onClick(View v) {
		if (!timerHasStarted) {
			countDownTimer.start();
			timerHasStarted = true;
			// startB.setText("STOP");
		} else {
			countDownTimer.cancel();
			timerHasStarted = false;
			// startB.setText("RESTART");
		}
	}

	public class MyCountDownTimer extends CountDownTimer {
		
		// Diff 5 - Different constructor and missing global var
		long starttime3 = 0;

		public MyCountDownTimer(long startTime2, long interval) {
			super(startTime2, interval);
			starttime3 = startTime2;
		}

		@Override
		public void onFinish() {
			timeDisplay.setText("Time's up!");
			AlertDialog alertDialog = new AlertDialog.Builder(TestPage.this)
					.create();

			TextView title = new TextView(context);
			title.setText("Aptitude App");
			title.setBackgroundColor(Color.DKGRAY);
			title.setPadding(10, 10, 10, 10);
			title.setGravity(Gravity.CENTER);
			title.setTextColor(Color.WHITE);
			title.setTextSize(20);
			alertDialog.setCustomTitle(title);

			// Setting Dialog Title
			// alertDialog.setTitle("Apptitude App");

			// Setting Dialog Message
			alertDialog.setMessage("TIME'S UP");

			// Setting Icon to Dialog
			alertDialog.setIcon(R.drawable.about);

			// Setting OK Button
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// Write your code here to execute after dialog closed
					// Diff 6 - different showscore class
					Intent i = new Intent(getApplicationContext(),
							ShowScore.class);
					time = timeDisplay.getText() + "";

					sec = sec + 40;
					String timetaken = min + "." + sec + "";

					double timetak = Float.parseFloat(timetaken);

					double tt = 20.00 - timetak;

					DecimalFormat df = new DecimalFormat("00.00");
					String j = df.format(tt);
					i.putExtra("tt", j);

					i.putExtra("score", buttonSelected);
					i.putExtra("givenans", solutions);
					i.putExtra("allid", questionIDs);
					i.putExtra("category", category);
					startActivity(i);
					TestPage.this.finish();

				}
			});

			// Showing Alert Message
			alertDialog.show();

		}

		@Override
		public void onTick(long millisUntilFinished) {
			int seconds = (int) (millisUntilFinished / 1000) % 60;
			int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
			timeTillFinish = millisUntilFinished;
			currentTime = timeTillFinish;

			min = minutes;
			sec = seconds;
			// tvTimerLabel.setVisibility(View.VISIBLE);
			timeDisplay.setText((minutes) + ":" + (seconds) + "");
			// text.setText("" + millisUntilFinished / 1000);

		}

	}
	
	

}
