package com.aptitudeguru.dashboard;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidhive.dashboard.R;

public class Psychometric extends AptitudeBaseActivity implements OnClickListener, android.text.TextWatcher {

	private AlertDialog alertDialog;
	final Context context = this;
	Button btn_next;
	Button btn_prev;
	int currentSituationIndex;
	TextView mainTextDisplay,timeDisplay;
	LinearLayout answerEntry;
	LinearLayout answerLabels;
	View answerBar;
	boolean developer = false;
	DatabaseHandler db = new DatabaseHandler(this);
	PsyScenario currentScenario;
	PsySituation currentSituation;
	EditText answer1; 
	EditText answer2; 
	
	MyCountDownTimer countDownTimer;
	private static final long STARTTIME = 60 * 5 * 1000;
	private static final long INTERVAL = 1 * 1000;
	private boolean timerOnPause=false;
	private long timeTillFinish = 0;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getIntent().getExtras();
		developer =  bundle.getBoolean("developer");
		if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) { 
	        // Activity was brought to front and not created, 
	        // Thus finishing this will get us to the last viewed activity 
	        finish(); 
	        return; 
	    } 
		setContentView(R.layout.scenario);
		currentSituationIndex = 0;
		implementFinishButton();
		implementNavigation();
		timeDisplay = (TextView) this.findViewById(R.id.timer);
		timeDisplay.setText( "5:00");
		startTestDialog();
		mainTextDisplay = (TextView) findViewById(R.id.textView1);
		answerEntry = (LinearLayout) findViewById(R.id.answer_entry);
		answerLabels = (LinearLayout) findViewById(R.id.answer_labels);
		answerBar = (View) findViewById(R.id.answer_Bar);
		currentScenario = db.getPsyScenario(1);
		currentSituation = currentScenario.getSituations().get(0);
		setPageText();
		answer1 = (EditText) findViewById(R.id.editText1);
		answer1.addTextChangedListener(this);
		answer2 = (EditText) findViewById(R.id.editText2);
		answer2.addTextChangedListener(this);
	}
	
	public void setDeveloper(boolean value)
	{
		developer = value;
	}
	
	public void setPageText()
	{
		String answersString = "";
	    char option = 'A';
		for (PsyAnswer currentAnswer : currentSituation.getAnswers())
		{
			answersString += "Option " + option + "\n" + currentAnswer.getAnswer() + "\n\n";
			option++;
		}

		String displayString = "Scenario 1\n\n" + currentScenario.getScenarioText()
		+ "\n\nSituation 1\n\n" + currentSituation.getSituationText()
		+ "\n\n" + answersString;
		
		mainTextDisplay.setText(displayString);
		//Perhaps a global which looks at the type of the page
		// 0 - scenario + situation
		// 1 - answer entry
		// Along with a variable which tracks which scenario and which situation
		// would be better than simple pagnation.
	}
	
	private void implementNavigation()
	{
		btn_next = (Button) findViewById(R.id.btn_next);
		btn_next.setEnabled(false);
		btn_next.setVisibility(View.INVISIBLE);
		btn_prev = (Button) findViewById(R.id.btn_prev);
		btn_prev.setEnabled(false);
		btn_prev.setVisibility(View.INVISIBLE);
	}
	
	private void implementFinishButton()
	 {
	  Button btn_finish = (Button) findViewById(R.id.btn_finish);
	  
	  btn_finish.setOnClickListener(new OnClickListener() {

	   @Override
	   public void onClick(View arg0) {
	   
	    if(!emptyAnswer(answer1.getText().toString()) && !emptyAnswer(answer2.getText().toString())){
	    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
	      context);
	    TextView title = new TextView(context);
	    title.setText("Aptitude App");
	    title.setBackgroundColor(Color.DKGRAY);
	    title.setPadding(10, 10, 10, 10);
	    title.setGravity(Gravity.CENTER);
	    title.setTextColor(Color.WHITE);
	    title.setTextSize(20);
	    alertDialogBuilder.setCustomTitle(title);

	    // set title
	    // alertDialogBuilder.setTitle();

	    // set dialog message
	    alertDialogBuilder
	      .setMessage("Click yes to finish test!")
	      .setCancelable(false)
	      .setPositiveButton("Yes",
	        new DialogInterface.OnClickListener() {
	         @Override
	         public void onClick(DialogInterface dialog,
	           int id) {
	        	 countDownTimer.cancel();
	          // if this button is clicked, close
	          // current activity
	          Intent i = new Intent(getApplicationContext(), ShowAnswers.class);
	          String mostLikely = ((EditText) findViewById(R.id.editText1)).getText().toString();
	          String leastLikely = ((EditText) findViewById(R.id.editText2)).getText().toString();
	          i.putExtra("mostLikely", mostLikely);
	          i.putExtra("leastLikely", leastLikely);
	          
	          startActivity(i);
	          Psychometric.this.finish();
	         }
	        })
	      .setNegativeButton("No",
	        new DialogInterface.OnClickListener() {
	         @Override
	         public void onClick(DialogInterface dialog,
	           int id) {
	          // if this button is clicked, just close
	          // the dialog box and do nothing
	          dialog.cancel();

	         }
	        });
	    // create alert dialog
	    AlertDialog alertDialog = alertDialogBuilder.create();
	    // show it
	    alertDialog.show();;
	   }
	    else
	    {
	    	EmptyEntryFinishAlert();
	    }
	   }
	  });
	 }
	
	
	private void startTestDialog()
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Psychometric.this);

		TextView title = new TextView(Psychometric.this);
		title.setText("Aptitude App");
		title.setBackgroundColor(Color.DKGRAY);
		title.setPadding(10, 10, 10, 10);
		title.setGravity(Gravity.CENTER);
		title.setTextColor(Color.WHITE);
		title.setTextSize(20);
		alertDialogBuilder.setCustomTitle(title);

		// set dialog message
		alertDialogBuilder
				.setMessage("Start Test?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {	
								dialog.cancel();
								countDownTimer=CountDownTimerSet();
								countDownTimer.start();
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing

						Psychometric.this.finish();
					}
				});
		// create alert dialog
		alertDialog = alertDialogBuilder.create();

		// show it
		if (!developer) {
			 alertDialog.show();
	        }
	}
	
	@Override
	  public void onDestroy() {
	    super.onDestroy();
	    if(alertDialog != null) {
	    	alertDialog.dismiss();
	    }
	}

	@Override
	public void onClick(View v) {
	}
	
	@Override
	public void onBackPressed() {
		Toast.makeText(getApplicationContext(), "You can not exit while a test is in progress",
				Toast.LENGTH_SHORT).show();
	}
	
	public MyCountDownTimer CountDownTimerSet()
	{
		if (developer)
		{
			countDownTimer = new MyCountDownTimer(STARTTIME, INTERVAL, timeDisplay, context, developer) ;
		}
		else
		{
			countDownTimer = new MyCountDownTimer(STARTTIME, INTERVAL, timeDisplay, context);
		}
		return countDownTimer;
	}

	public void setPagnation(int pagnation)
	{
		setPageText();
	}
	
	public CharSequence getMainTextDisplayText()
	{
		return mainTextDisplay.getText();
	}
	
	public int getAnswerEntryVisibility()
	{
		return answerEntry.getVisibility(); 
	}

	public int getAnswerLabelsVisibility() {
		return answerLabels.getVisibility();
	}

	public int getAnswerBarVisibility() {
		return answerBar.getVisibility();
	}
	
	private boolean userInputValidation() {
		String[] answersArray = {answer1.getText().toString(), answer2.getText().toString()};
		int[] id = { R.id.editText1, R.id.editText2 };
		for(int i = 0; i < answersArray.length; i++)
		{
			if(!emptyAnswer(answersArray[i]))
			{
				if(!validInput(answersArray[i]) || containsDuplicates(answersArray))
				{
					EditText notvalid = (EditText) findViewById(id[i]);
					notvalid.setText("");
					WrongInputAlert();
					return false;
				}
			}
		}
		return true;
	}
	
	public Boolean emptyAnswer(String input) {
		if(input.equals("") || input == null)
		{
			return true;
		}
		return false;
	}

	public Boolean validInput(String input) {
		String text = input.toLowerCase();
		if(text.equals("a") || text.equals("b") || text.equals("c") || text.equals("d"))
		{
			return true;
		}
		return false;
	}

	public Boolean containsDuplicates(String [] answers) {
		for(int i = 0; i < answers.length; i++)
		{
			answers[i] = answers[i].toLowerCase();
		}
		List<String> answerList = Arrays.asList(answers);
		Set<String> answerSet = new HashSet<String>(answerList);
		if(answerSet.size() < answerList.size())
		{
			return true;
		}
		return false;
	}
	
	 private void WrongInputAlert()
	 {
	if(!developer)
	{
	  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
	    context);
	  TextView title = new TextView(context);
	  title.setText("Aptitude App");
	  title.setBackgroundColor(Color.DKGRAY);
	  title.setPadding(10, 10, 10, 10);
	  title.setGravity(Gravity.CENTER);
	  title.setTextColor(Color.WHITE);
	  title.setTextSize(20);
	  alertDialogBuilder.setCustomTitle(title);
	  alertDialogBuilder
	    .setMessage("Answers should only contain A, B, C, or D and should not contain duplicates")
	    .setCancelable(false)
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	              public void onClick(DialogInterface dialog, int id) {
	           
	              }
	          });
	    AlertDialog alertDialog = alertDialogBuilder.create();
	    alertDialog.show();
	   }
	 }

	 private void EmptyEntryFinishAlert()
	 {
		 if(!developer)
		 {
		  AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
		    context);
		  TextView title = new TextView(context);
		  title.setText("Aptitude App");
		  title.setBackgroundColor(Color.DKGRAY);
		  title.setPadding(10, 10, 10, 10);
		  title.setGravity(Gravity.CENTER);
		  title.setTextColor(Color.WHITE);
		  title.setTextSize(20);
		  alertDialogBuilder.setCustomTitle(title);
		  alertDialogBuilder
			.setMessage("Are you sure you want to quit test without answering?")
			.setCancelable(false)
			.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {	
							Psychometric.this.finish();
						}
					})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
				
				}
			});
		    AlertDialog alertDialog = alertDialogBuilder.create();
		    alertDialog.show();
		 }
	   }

	public Boolean isCorrectLength(String[] input, int requiredLength) {
		if(input.length == requiredLength)
		{
			return true;
		}
		return false;
	}

	public Boolean validAnswers(String[] answers) {
		if(isCorrectLength(answers, 2) && !containsDuplicates(answers))
		{
			for(int i = 0; i < answers.length; i++)
			{
				if(emptyAnswer(answers[i]) || !validInput(answers[i]))
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {	
	}

	@Override
	public void afterTextChanged(Editable s) {
		userInputValidation();
	}
}
