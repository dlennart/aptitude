package com.aptitudeguru.dashboard;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidhive.dashboard.R;

public class AptitudeBaseActivity extends Activity {

	public ArrayList<Button> createMainMenu()
	{
		ArrayList<Button> menuButtons = new ArrayList<Button>();
		menuButtons.add((Button) findViewById(R.id.btn_home));
		menuButtons.add((Button) findViewById(R.id.btn_fav));
		menuButtons.add((Button) findViewById(R.id.btn_score));
		menuButtons.add((Button) findViewById(R.id.btn_soundon));//Refactor this to be tutorial?? :s
		menuButtons.add((Button) findViewById(R.id.btn_about));
		menuButtons.add((Button) findViewById(R.id.btn_help));
		Class<?>[] menuClasses = {AndroidDashboardDesignActivity.class, FavPage.class, scoremenu.class, Tutorialpage.class, AboutUs.class, Help.class};

		for (int k = 0; k< menuButtons.size(); k++)
		{
			menuButtons.get(k).setTag(menuClasses[k]);
			menuButtons.get(k).setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View view) {
					Intent i = new Intent(getApplicationContext(), (Class<?>)view.getTag());
					if((Class<?>)view.getTag() == AndroidDashboardDesignActivity.class)
					{
						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					}
					startActivity(i);
				}
			});
		}
		return menuButtons;
	}
}
