package com.aptitudeguru.dashboard;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.os.Build;

public class StringReplacer {
	
	HashMap<String, String> dictionaryMap;
	String dictionaryLocale;
	
	private final static String[] metricRegEx = {"\\%km\\%","\\%kilometers\\%", "\\%kilometer\\%", "\\%kmph\\%", "\\%m\\%", "\\%litre\\%", "\\%litres\\%", "\\%l\\%", "\\%ml\\%", "\\%kg\\%", "\\%cm\\%", "\\%centimeters\\%", "\\%mm\\%", "\\%acres\\%", "\\%acre\\%", "\\%g\\%", "\\%kilograms\\%"};
	private final static String[] metric = {"km","kilometers", "kilometer", "kmph", "m", "litre", "litres", "l", "ml", "kg", "cm", "centimeters", "mm", "acres", "acres", "g", "kilograms"};
	private final static String[] imperialRegEx = {"\\%mi\\%","\\%miles\\%", "\\%mile\\%", "\\%mph\\%", "\\%ft\\%", "\\%pint\\%", "\\%pints\\%", "\\%pt\\%", "\\%fl oz\\%", "\\%st\\%", "\\%in\\%", "\\%inches\\%", "\\%4/10 in\\%", "\\%hectares\\%", "\\%hectare\\%", "\\%oz\\%", "\\%pounds\\%"};
	private final static String[] imperial= {"mi","miles", "mile", "mph", "ft", "pint", "pints", "pt", "fl oz", "st", "in", "inches", "4/10 in", "hectares", "hectare", "oz", "pounds"};
	
	public StringReplacer()
	{
		dictionaryMap = new HashMap<String, String>();
		PhoneLocale pl = new PhoneLocale();
		dictionaryLocale = pl.getCurrentCountry();
		String symbol = pl.getSymbol();
		String currencyName = pl.getCurrencyName();
		String units = pl.getUnits();
		populateDictionary(symbol,currencyName,units);
	}

	public StringReplacer(String symbol, String currencyName, String unit)
	{
		dictionaryMap = new HashMap<String, String>();
		PhoneLocale pl = new PhoneLocale();
		dictionaryLocale = pl.getCurrentCountry();
		populateDictionary(symbol,currencyName,unit);
//		PhoneLocale pl = new PhoneLocale();
//		String symbol = Currency.getInstance(pl.getLocale()).getSymbol();
//		String currencyName = pl.getCurrency();
	}
	
	public HashMap<String, String> getMap() {
		
		return dictionaryMap;
	}

	public Set<String> getHashMapKeys()
	{
		return dictionaryMap.keySet();
	}
	
	public void addToDictionary(String key, String value) {
		dictionaryMap.put(key, value);
	}

	public String getFromDictionary(String key) {
		return dictionaryMap.get(key);
	}
	
	private void populateDictionary(String symbol, String currencyName, String units)
	{
		addToDictionary("\\$Rs\\.\\$",symbol);
		addToDictionary("\\$Rupees\\$",currencyName + "s");
		addToDictionary("\\$Rupee\\$",currencyName);
		if (units.equals("metric"))
		{
			for (int i=0; i<imperialRegEx.length; i++ )
			{
				addToDictionary(imperialRegEx[i], metric[i]);
				addToDictionary(metricRegEx[i], metric[i]);
			}
			addToDictionary("\\%gm\\%", "gm");
		}
		else
		{
			for (int i=0; i<metricRegEx.length; i++)
			{
				addToDictionary(metricRegEx[i], imperial[i]);
				addToDictionary(imperialRegEx[i], imperial[i]);
			}
			addToDictionary("\\%gm\\%", "oz");
		}
	}
	
	public String convertStringWithreplacement(String toConvert)
	{
		String replaced = toConvert;
		ArrayList<String> expression = new ArrayList<String>(getHashMapKeys());
		RegExpMatching rx; 
		for(int i = 0; i < expression.size(); i++)
		{
			rx = new RegExpMatching(expression.get(i));
			replaced = rx.expressionReplace(replaced, getFromDictionary(expression.get(i)));
		}
		return replaced;
	}

	public boolean existsInDictionary(String key) {
		return dictionaryMap.containsKey(key);
	}
}
