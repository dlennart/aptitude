package com.aptitudeguru.dashboard;

public class DBMSTable extends QuestionTable{

	public DBMSTable()
	{
		super();
		tableCat = "dbms";
		key_questID = "dbmsid";
		key_quest = "dbmsques";
	}
}
