package androidhive.dashboard.test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import com.aptitudeguru.dashboard.Quantitative;
import com.aptitudeguru.dashboard.ShowScore;
import android.R.integer;
import android.content.Intent;
import android.graphics.Color;
import android.test.ActivityUnitTestCase;
import android.widget.Button;
import androidhive.dashboard.R;
import static org.junit.Assert.*;

public class TQuantatitiveTests extends ActivityUnitTestCase<Quantitative> {
	
	private Quantitative quantitativeActivity;
	ArrayList<Button> dummyButtons;
	ArrayList<Button> menuButtons;
			
	public TQuantatitiveTests()
	{
		super(Quantitative.class);		
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Intent intent = new Intent(getInstrumentation().getTargetContext(), Quantitative.class);
		startActivity(intent, null, null);
		quantitativeActivity = getActivity();
		menuButtons = quantitativeActivity.createMainMenu();
	}

	public void testCreateMainMenu() {
		assertNotNull("should not be null", menuButtons);
	}
	
	public void testCreateMainMenuTotal()
	{
		assertEquals("Should have 6 values in the array", 6, menuButtons.size());
	}
	
	public void testSubCatergoryButtons()
	{
		menuButtons = quantitativeActivity.createSubCategoryButtons();
		assertNotNull("should not be null", menuButtons);
	}
	
	public void testSubCatergoryButtonsTotal()
	{
		menuButtons = quantitativeActivity.createSubCategoryButtons();
		assertEquals("Should have 14 values in the array", 14, menuButtons.size());
	}

}

