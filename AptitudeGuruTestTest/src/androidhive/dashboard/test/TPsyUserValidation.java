package androidhive.dashboard.test;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aptitudeguru.dashboard.Psychometric;

public class TPsyUserValidation extends TestCase {
	
	private Psychometric p;

	
	@Before
	public void setUp() throws Exception {
		p = new Psychometric();
		p.setDeveloper(true);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInputNotNull() {
		Boolean isEmpty = p.emptyAnswer("");
		assertTrue("The string should not be empty", isEmpty);
	}
	
	@Test
	public void testValidInput()
	{
		String answer1 = "a";
		String answer2 = "B";
		Boolean isValid = p.validInput(answer1);
		assertTrue("The string a should be valid", isValid);
		isValid = p.validInput(answer2);
		assertTrue("The string B should be valid", isValid);
	}
	
	@Test
	public void testValidAnswers()
	{
		String [] answers = {"a", "B"};
		Boolean isValid = p.validAnswers(answers);
		assertTrue("Answers should be valid in all ways", isValid);
	}
	
	@Test 
	public void testInvalidInput()
	{
		String answer1 = "y";
		String answer2 = "2";
		String answer3 = "Y";
		Boolean isValid = p.validInput(answer1);
		assertTrue("The string should not be valid", !isValid);
		isValid = p.validInput(answer2);
		assertTrue("The string should not be valid", !isValid);
		isValid = p.validInput(answer3);
		assertTrue("The string should not be valid", !isValid);
	}
	
	@Test 
	public void testBoundryMinLowerCase()
	{
		assertTrue("The min valid entry -1 should not be valid", !p.validInput("`"));
	}
	
	@Test
	public void testBoundryMaxLowerCase()
	{
		assertTrue("The max valid entry -1 should not be valid", !p.validInput("e"));
	}
	
	@Test 
	public void testBoundryMinUpperCase()
	{
		assertTrue("The min valid entry -1 should not be valid", !p.validInput("@"));
	}
	
	@Test
	public void testBoundryMaxUpperCase()
	{
		assertTrue("The max valid entry -1 should not be valid", !p.validInput("E"));
	}
	
	@Test
	public void testDuplicates()
	{
		String [] answers = {"a", "A"};
		Boolean duplicates = p.containsDuplicates(answers);
		assertTrue("The answers should contain duplicates", duplicates);
	}
	
	@Test 
	public void testInputsTotal()
	{
		String [] test = new String[]{"a", "b"};
		Boolean isLengthTwo = p.isCorrectLength(test, 2);
		assertTrue("The array should be of size 2", isLengthTwo);
	}
	
	@Test 
	public void testInputsTotalMaxBoundry()
	{
		String [] test = new String[]{"a", "b", "c"};
		Boolean isLengthTwo = p.isCorrectLength(test, 2);
		assertTrue("The array should be of size 2", !isLengthTwo);
	}
	
	@Test 
	public void testInputsTotalMinBoundry()
	{
		String [] test = new String[]{"a"};
		Boolean isLengthTwo = p.isCorrectLength(test, 2);
		assertTrue("The array should be of size 2", !isLengthTwo);
	}
	
	@Test 
	public void testInputsTotalInvalid()
	{
		String [] test = new String[]{"a", "b", "c", "d"};
		Boolean isLengthTwo = p.isCorrectLength(test, 2);
		assertTrue("The array should be of size 2", !isLengthTwo);
	}
}
