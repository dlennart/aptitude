package androidhive.dashboard.test;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import com.aptitudeguru.dashboard.RegExpMatching;

public class TCRegExCurrencyTests extends TestCase {

	RegExpMatching currencyRegExp;
	RegExpMatching currencyNameRegExp;
	RegExpMatching milesRegExp;
	
	@Before
	public void setUp() throws Exception {
		currencyRegExp = new RegExpMatching("\\$Rs\\.\\$");
		currencyNameRegExp = new RegExpMatching("\\$Rupees\\$");
		milesRegExp = new RegExpMatching("\\%miles\\%");
		
	}

	
	@Test
	public void testMatchCurrencyExpressionOnly()
	{
		String testString1 = "$Rs.$";
		String testString2 = "$Rupees$";
		String testString3 = "%miles%";
		
		assertTrue("String should match on $Rs.$",currencyRegExp.expressionMatch(testString1));
		assertTrue("String should match on $Rupees$",currencyNameRegExp.expressionMatch(testString2));
		assertTrue("String should match on %miles%",milesRegExp.expressionMatch(testString3));
	}
	
	@Test 
	public void testMatchCurrencyCaseInsensitive()
	{
		String testString1 = "$RS.$";
		String testString2 = "There are $RS.$ in this statement";
		String testString3 = "$Rs.$";
		String testString4 = "There are $Rs.$ in this statement";
		String testString5 = "$Rupees$";
		String testString6 = "There are $Rupees$ in this statement";
		String testString7 = "$rupees$";
		String testString8 = "There are $rupees$ in this statement";
		
		assertEquals("Result should be the same regardless of case on $Rs.$", currencyRegExp.expressionMatch(testString1), currencyRegExp.expressionMatch(testString3));
		assertEquals("Result should be the same regardless of case on $Rs.$", currencyRegExp.expressionMatch(testString2), currencyRegExp.expressionMatch(testString4));
		assertEquals("Result should be the same regardless of case on $Rupees$", currencyNameRegExp.expressionMatch(testString5), currencyNameRegExp.expressionMatch(testString7));
		assertEquals("Result should be the same regardless of case on $Rupees$", currencyNameRegExp.expressionMatch(testString6), currencyNameRegExp.expressionMatch(testString8));
	}
	
	@Test
	public void testMatchCurrencyMixed()
	{
		String testString1 = "There are $Rs.$ in this statement";
		String testString2 = "There are $Rupees$ in this statement";
		
		assertTrue("String should match on $Rs.$",currencyRegExp.expressionMatch(testString1));
		assertTrue("String should match on $Rupees$",currencyNameRegExp.expressionMatch(testString2));
	}
	
	@Test
	public void testDoesNotMatchExpression()
	{
		String testString1 = "No currency";
		String testString2 = "There are $$ in this statement";
		String testString3 = "$somethingRs.$";
		assertFalse("String should not match on $Rs.$",currencyRegExp.expressionMatch(testString1));
		assertFalse("String should not match on $Rs.$",currencyRegExp.expressionMatch(testString2));
		assertFalse("String should not match on $Rs.$",currencyRegExp.expressionMatch(testString3));
		assertFalse("String should not match on $Rupees$",currencyNameRegExp.expressionMatch(testString1));
		assertFalse("String should not match on $Rupees$",currencyNameRegExp.expressionMatch(testString2));
		assertFalse("String should not match on $Rupees$",currencyNameRegExp.expressionMatch(testString3));
	}
	
	@Test
	public void testSplitCurrencyExpression()
	{
		String testString1 = "Before $Rs.$ After";
		String testString2 = "Before$Rs.$After";
		String testString3 = "$Rs.$After";
		String testString4 = "Before$Rs.$";
		String testString5 = "There are several $Rs.$ in this $Rs.$ string";
		String testString6 = "Before $Rupees$ After";
		String testString7 = "Before$Rupees$After";
		String testString8 = "$Rupees$After";
		String testString9 = "Before$Rupees$";
		String testString10 = "There are several $Rupees$ in this $Rupees$ string"; 
		
		assertArrayEquals("Arrays do not match", new String[] {"Before ", " After"}, currencyRegExp.expressionSplit(testString1));
		assertArrayEquals("Arrays do not match",new String[] {"Before", "After"}, currencyRegExp.expressionSplit(testString2));
		assertArrayEquals("Arrays do not match",new String[] {"","After"}, currencyRegExp.expressionSplit(testString3));
		assertArrayEquals("Arrays do not match",new String[] {"Before"}, currencyRegExp.expressionSplit(testString4));
		assertArrayEquals("Arrays do not match",new String[] {"There are several ", " in this ", " string"}, currencyRegExp.expressionSplit(testString5));
		assertArrayEquals("Arrays do not match",new String[] {"Before ", " After"}, currencyNameRegExp.expressionSplit(testString6));
		assertArrayEquals("Arrays do not match",new String[] {"Before", "After"}, currencyNameRegExp.expressionSplit(testString7));
		assertArrayEquals("Arrays do not match",new String[] {"","After"}, currencyNameRegExp.expressionSplit(testString8));
		assertArrayEquals("Arrays do not match",new String[] {"Before"}, currencyNameRegExp.expressionSplit(testString9));
		assertArrayEquals("Arrays do not match",new String[] {"There are several ", " in this ", " string"}, currencyNameRegExp.expressionSplit(testString10));
	}
	
	@Test
	public void testReplaceMatchingCurrency()
	{
		String testString1 = "$Rs.$";
		String testString2 = "$Rupees$";
		
		assertEquals("$ should be returned","$",currencyRegExp.expressionReplace(testString1, "\\$"));
		assertEquals("dollars should be returned","dollars",currencyNameRegExp.expressionReplace(testString2, "dollars"));
	}
	
	@Test
	public void testReplaceNoMatchingCurrency()
	{
		String testString1 = "There is nothing here to replace";
		
		assertEquals("The string should not be modified",testString1,currencyRegExp.expressionReplace(testString1, "\\$"));
		assertEquals("The string should not be modified",testString1,currencyNameRegExp.expressionReplace(testString1, "dollars"));
	}
}
