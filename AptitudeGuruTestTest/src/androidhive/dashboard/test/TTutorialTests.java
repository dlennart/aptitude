package androidhive.dashboard.test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import com.aptitudeguru.dashboard.Tutorialpage;
import com.aptitudeguru.dashboard.ShowScore;
import android.R.integer;
import android.content.Intent;
import android.graphics.Color;
import android.test.ActivityUnitTestCase;
import android.widget.Button;
import androidhive.dashboard.R;
import static org.junit.Assert.*;

public class TTutorialTests extends ActivityUnitTestCase<Tutorialpage> {
	
	private Tutorialpage tutorialPageActivity;
	ArrayList<Button> dummyButtons;
	ArrayList<Button> tutorialButtons;
			
	public TTutorialTests()
	{
		super(Tutorialpage.class);		
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Intent intent = new Intent(getInstrumentation().getTargetContext(), Tutorialpage.class);
		startActivity(intent, null, null);
		tutorialPageActivity = getActivity();
		dummyButtons = new ArrayList<Button>();
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_quants));
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_c));
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_cpp));
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_java));
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_os));
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_dbms));
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_datastructure));
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_html));
		dummyButtons.add((Button) tutorialPageActivity.findViewById(R.id.btn_score_interview));
		tutorialButtons = tutorialPageActivity.createTutorialButtons();
	}

	public void testTutorialMenuNotNull() {
		assertNotNull("Tutorial buttons should not be null", tutorialButtons);
	}
	
	public void testTutorialMenuIsCorrectSize()
	{
		assertEquals("Tutorial Menu should have 9 buttons", 9, tutorialButtons.size());
	}
	
	public void testTutorialMenuButtonsMatch()
	{
		for (int i = 0; i < tutorialButtons.size(); i++)
		{
			assertEquals("Created tutorial button does not match intended button",tutorialButtons.get(i), dummyButtons.get(i));
		}
	}

}
