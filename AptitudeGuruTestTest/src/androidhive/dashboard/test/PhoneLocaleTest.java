package androidhive.dashboard.test;

import static org.junit.Assert.*;

import java.util.Currency;
import java.util.Locale;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.os.Build;

import com.aptitudeguru.dashboard.PhoneLocale;

public class PhoneLocaleTest extends TestCase {
	//String country = current.getCurrentCountry();
	//Locale loc = Resources.getSystem().getConfiguration().locale;
	
	PhoneLocale current;
	Locale testLocale;
	PhoneLocale plUK = new PhoneLocale(Locale.UK);
	PhoneLocale plCH = new PhoneLocale(Locale.CHINA);
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		current = new PhoneLocale();
		testLocale = Resources.getSystem().getConfiguration().locale;
	}

	@Test
	public void testCountrySet() {
		//fail("Not yet implemented");
		assertNotNull("Locale should not be empty", current.getCurrentCountry());	
	}
	
	public void testCountryCorrect(){//Maybe needs changed
		String country = current.getCurrentCountry();
		assertTrue("Country should be the same as test Locale, got "+ country +".",
				testLocale.getCountry().equals(country));
	}
	
	@Test
	public void testLocaleSet(){
		assertNotNull("Locale should be set", current.getLocale());
	}
	
	@Test
	public void testCurrency(){
		
		if(android.os.Build.VERSION.SDK_INT > 18)
		{
			assertEquals("Currency name should be British Pound","British Pound", plUK.getCurrencyName());
			assertEquals("Currency name should be Chinese Yuan","Chinese Yuan",plCH.getCurrencyName());
		}
		else
		{
			assertEquals("Currency name should be pound","pound",plUK.getCurrencyName());
			assertEquals("Currency name should be CNY","CNY",plCH.getCurrencyName());
		}
	}
	
	@Test
	public void testCurrencyCode(){
		assertEquals("Currency Code should be GBP", "GBP", plUK.getCurrencyCode());
		assertEquals("Currency Code should be CNY","CNY", plCH.getCurrencyCode());
	}
	
	
	@Test
	public void testCurrencySymbol()
	{
		assertEquals("Currency Symbol should be �","�",plUK.getSymbol());
		assertEquals("Currency Symbol should be �","CN�",plCH.getSymbol());
	}
	
	@Test
	public void testUnitsValid(){
		String units = current.getUnits();
		assertTrue("Units can be Imperial or Metric, got "+ units+" ",
				units.equals("imperial")||
				units.equals("metric"));
	}
	
	@Test
	public void testUnitsByCountry(){
		String units = current.getUnits();
		String country = current.getCurrentCountry();
		if (country.equals("GB") || country.equals("US")){
			assertEquals("If country is US|GB, units should be imperial", units, "imperial");
		}
		else {
			assertEquals("Units should be metric", units, "metric");
		}
	}

}
