package androidhive.dashboard.test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import com.aptitudeguru.dashboard.ShowScore;

import android.R.integer;
import android.content.Intent;
import android.graphics.Color;
import android.test.ActivityUnitTestCase;
import android.widget.Button;
import androidhive.dashboard.R;
import static org.junit.Assert.*;

public class TShowScoretest extends ActivityUnitTestCase<ShowScore> {

	private ShowScore showScoreActivity;
	private Random r;
	private int[] sampleAnswers = {1,1,1,1,1,1,1,1,1,1};
	ArrayList<Button> dummyButtons;
	ArrayList<Button> scoreButtons;
			
	public TShowScoretest()
	{
		super(ShowScore.class);
	}
	
	@Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(), ShowScore.class);
        				
        double tt = 20.00;
        DecimalFormat df = new DecimalFormat("00.00");
        String j = df.format(tt);
        intent.putExtra("tt", j);

        int[] ans = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
        int[] givenans = ans;
        int[] a = ans;

        intent.putExtra("score", ans);
        intent.putExtra("givenans", givenans);
        intent.putExtra("allid", a);
        intent.putExtra("category", "q1");
        startActivity(intent, null, null);
        showScoreActivity = getActivity();
        r = new Random();
        
        scoreButtons = showScoreActivity.createScoreButtons();
        
        dummyButtons = new ArrayList<Button>();
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score1));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score2));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score3));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score4));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score5));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score6));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score7));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score8));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score9));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score10));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score11));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score12));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score13));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score14));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score15));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score16));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score17));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score18));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score19));
		dummyButtons.add((Button) showScoreActivity.findViewById(R.id.score20));
        
   /*     final MainActivity a = getActivity();
        // ensure a valid handle to the activity has been returned
        assertNotNull(a);*/
    }
	public void testColourAssigmentNotNull()
	{
		for (int i = 1; i<1000; i++)
		{
			int sol = r.nextInt(i);
			int ans = r.nextInt(i);
			assertNotNull("Color assigned was null",showScoreActivity.colourAssignment(sol,ans));
		}
	}
	
	public void testMatchingReturnsGreen()
	{
		assertEquals("Color assigned was not green",Color.GREEN,showScoreActivity.colourAssignment(1, 1));
	}
	
	public void testNonMatchingReturnsRed()
	{
		assertEquals("Color assigned was not red",Color.RED,showScoreActivity.colourAssignment(2, 1));
	}
	
	public void testNonAnswerReturnsBlue()
	{
		assertEquals("Color assigned was not blue",Color.BLUE,showScoreActivity.colourAssignment(3,0));
	}
	
	public void testScoreCalculatorReturnsValue()
	{		
		int[] calculatedTotal = showScoreActivity.calculateTotalScore(sampleAnswers,sampleAnswers);
		assertNotNull("Calculated Values of Score and Incorrect was Null.",calculatedTotal);
	}
	
	public void testIntArrayReturnedByScoreCalculator()
	{
		int[] calculatedTotal = showScoreActivity.calculateTotalScore(sampleAnswers,sampleAnswers);
		assertSame((new int[]{}).getClass(), calculatedTotal.getClass());
	}
	
	public void testCalculatedScoreNotGreaterThanNumberOfSolutions()
	{
		int n = 10;
		for (int i = 0; i<1000; i++)
		{
			n = r.nextInt(50);
			int[] sols = generateSampleArray(n);
			int[] answers = generateSampleUserAnswers(n);
			int[] calculatedTotals = showScoreActivity.calculateTotalScore(sols, answers);
			assertFalse("Number of calculated correct answers is greater than number of questions",calculatedTotals[0] > n);
			assertFalse("Number of calculated incorrect answers is greater than number of questions",calculatedTotals[1] > n);
			assertFalse("Number of calculated correct and incorrect answers is greater than number of questions",(calculatedTotals[0] + calculatedTotals[1]) > n);
		}
	}
	
	public void testScoreCalculatorAccurate()
	{
		int n = 10;
		for (int i = 0; i<1000; i++)
		{
			int[] sols = generateSampleArray(n);
			int[] answers = generateSampleUserAnswers(n);
			int[] calculatedTotals = showScoreActivity.calculateTotalScore(sols, answers);
			int[] correctTotals = arrayComparison(sols, answers);
			
			assertEquals("Calculated correct answers is incorrect",calculatedTotals[0], correctTotals[0]);
			assertEquals("Calculated incorrect answers is incorrect",calculatedTotals[1], correctTotals[1]);
			assertEquals("Calculated incomplete answers is incorrect",n - (calculatedTotals[0] + calculatedTotals[1]), correctTotals[2]);
		}
	}
	
	
	
	private int[] generateSampleArray(int size)
	{
		int[] sampleArray = new int[size];
		for (int i = 0; i < size; i++)
		{
			sampleArray[i] = r.nextInt(3)+1;
		}
		return sampleArray;
	}
	
	private int[] generateSampleUserAnswers(int size)
	{
		int[] sampleArray = new int[size];
		for (int i = 0; i < size; i++)
		{
			sampleArray[i] = r.nextInt(4);
		}
		return sampleArray;
	}
	
	private int[] arrayComparison(int[] sols, int[] userAns)
	{
		int score = 0, incorrect = 0, incomplete = 0;
		for (int i = 0; i < sols.length; i++)
		{
			if (userAns[i] == 0)
			{
				incomplete++;
			}
			else if (sols[i] == userAns[i])
			{
				score++;
			}
			else
			{
				incorrect++;
			}
		}
		int[] totals = {score,incorrect,incomplete};
		return totals;
	}
	
	public void testScoreButtonsNumbers()
	{
		assertEquals("There should be twenty buttons in the array list", 20, scoreButtons.size());
	}
	
	public void testScoreButtonsAreCorrect()
	{
		for (int i = 0; i < scoreButtons.size(); i++)
		{
			assertEquals("Created button does not match intended button",scoreButtons.get(i), dummyButtons.get(i));
		}
	}
	
//	public void testScoreButtonNumbers()
//	{
//		ArrayList<Button> scoreButtons = showScore.createScoreButtons();
//		assertNotNull(scoreButtons);
//	}
	
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}
	
}


