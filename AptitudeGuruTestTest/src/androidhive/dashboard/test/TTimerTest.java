package androidhive.dashboard.test;
import android.app.Activity;
import android.os.CountDownTimer;
import com.aptitudeguru.dashboard.TimerActivity;
import junit.framework.TestCase;
public class TTimerTest extends TestCase {

	TimerActivity ta = new TimerActivity();
	private long startTime;
	private long interval;
	private CountDownTimer cd2;
	
	
	protected void setUp() throws Exception {
		super.setUp();
		startTime = 120 * 1000;
		interval = 1 * 1000;
		cd2 = null;
		cd2 = ta.CountDownTimerCreation(startTime, interval, cd2);
		
	}
	
	public void testCountDownTimerCreated() 
	{
		assertNotNull("CountDownTimer should exist", cd2);
	}
	
	public void testStartTime()
	{
		assertEquals("CountdownTimer startTime should be 120 * 1000 ms", ta.getStartTime(), (120 * 1000));
	}
	
	public void testInterval()
	{
		assertEquals("CountdownTimer interval should be 1 * 1000 ms", ta.getInterval(), (1 * 1000));
	}
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public class MyCountDownTimer2 extends CountDownTimer {
		  public MyCountDownTimer2(long startTime, long interval) {
		   super(startTime, interval);
		  }
		  
		  @Override
		  public void onFinish() {
		  }
		
		  @Override
		  public void onTick(long millisUntilFinished) {		
		  }
	}

}
