package androidhive.dashboard.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import android.content.res.Resources;

import com.aptitudeguru.dashboard.PhoneLocale;
import com.aptitudeguru.dashboard.RegExpMatching;
import com.aptitudeguru.dashboard.StringReplacer;

public class TStringReplacerTest extends TestCase {

	StringReplacer replacer;
	private final static String[] metricRegEx = {"\\%kilometers\\%", "\\%kilometer\\%", "\\%kmph\\%", "\\%m\\%", "\\%litre\\%", "\\%litres\\%", "\\%l\\%", "\\%ml\\%", "\\%kg\\%", "\\%cm\\%", "\\%centimeters\\%", "\\%mm\\%", "\\%acres\\%", "\\%acre\\%", "\\%g\\%", "\\%kilograms\\%"};
	private final static String[] metric = {"kilometers", "kilometer", "kmph", "m", "litre", "litres", "l", "ml", "kg", "cm", "centimeters", "mm", "acres", "acres", "g", "kilograms"};
	private final static String[] imperialRegEx = {"\\%miles\\%", "\\%mile\\%", "\\%mph\\%", "\\%ft\\%", "\\%pint\\%", "\\%pints\\%", "\\%pt\\%", "\\%fl oz\\%", "\\%st\\%", "\\%in\\%", "\\%inches\\%", "\\%4/10 in\\%", "\\%hectares\\%", "\\%hectare\\%", "\\%oz\\%", "\\%pounds\\%"};
	private final static String[] imperial= {"miles", "mile", "mph", "ft", "pint", "pints", "pt", "fl oz", "st", "in", "inches", "4/10 in", "hectares", "hectare", "oz", "pounds"};
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		replacer = new StringReplacer();
	}
	
	@Test
	public void testAddingToDictionaryMap()
	{
		replacer.addToDictionary("Current","New");
		assertEquals("The key 'Current' with the value 'New' should be added to the dictionary","New",replacer.getFromDictionary("Current"));
	}
	
	@Test
	public void testGetHashMapKeys()
	{
		Set<String> fixedSet = new HashSet<String>();
		fixedSet.add("\\$Rs\\.\\$");
		fixedSet.add("\\$Rupees\\$");
		Set<String> newSet = replacer.getHashMapKeys();
		assertTrue(newSet.containsAll(fixedSet));
	}
	
	@Test
	public void testAddingToDictionaryMapWithLocale()
	{
		Locale locale = Resources.getSystem().getConfiguration().locale;
		Currency currency = Currency.getInstance(locale);
		String symbol = currency.getSymbol();
		replacer.addToDictionary("\\$Rs\\.\\$", symbol);
		assertEquals("The key '\\$Rs\\.\\$' with the value '"+ symbol + "' should be added to the dictionary",symbol,replacer.getFromDictionary("\\$Rs\\.\\$"));
	}
	
	@Test
	public void testDictionaryForStandardValues()
	{
		PhoneLocale ph = new PhoneLocale();
		Locale locale = Resources.getSystem().getConfiguration().locale;
		Currency currency = Currency.getInstance(locale);
		String symbol = currency.getSymbol();
		String currencyName = ph.getCurrencyName();
		assertEquals("The key '\\$Rs\\.\\$' with the value '"+ symbol + "' should be added to the dictionary",symbol,replacer.getFromDictionary("\\$Rs\\.\\$"));
		assertEquals("The key '\\$Rupees\\$' with the value '"+ currencyName + "' should be added to the dictionary",currencyName + "s",replacer.getFromDictionary("\\$Rupees\\$"));
		//assertEquals("The key 'kilometers' with the value 'miles' should be added to the dictionary","miles",replacer.getFromDictionary("kilometers"));
	}
	
	@Test
	public void testUSLocaleDictionary()
	{
		StringReplacer sr = new StringReplacer("$","dollar","imperial");
		assertEquals("The key '\\$Rs\\.\\$' with the value '$' should be added to the dictionary","$",sr.getFromDictionary("\\$Rs\\.\\$"));
		assertEquals("The key '\\$Rupees\\$' with the value 'dollars' should be added to the dictionary","dollars",sr.getFromDictionary("\\$Rupees\\$"));
		for (int i =0; i < metricRegEx.length; i++)
		{
			assertEquals("The key '"+ metricRegEx[i] + "' with the value '" + imperial[i] +"' should be added to the dictionary",imperial[i], sr.getFromDictionary(metricRegEx[i]));
		}
	
		for (int i =0; i < imperialRegEx.length; i++)
		{
			assertEquals("The key '"+ imperialRegEx[i] + "' with the value '" + imperial[i] +"' should be added to the dictionary",imperial[i], sr.getFromDictionary(imperialRegEx[i]));
		}
	}
	
	@Test
	public void testUKLocaleDictionary()
	{
		StringReplacer sr = new StringReplacer("�","pound","imperial");
		assertEquals("The key '\\$Rs\\.\\$' with the value '�' should be added to the dictionary","�",sr.getFromDictionary("\\$Rs\\.\\$"));
		assertEquals("The key '\\$Rupees\\$' with the value 'pounds' should be added to the dictionary","pounds",sr.getFromDictionary("\\$Rupees\\$"));
		for (int i =0; i < metricRegEx.length; i++)
		{
			assertEquals("The key '"+ metricRegEx[i] + "' with the value '" + imperial[i] +"' should be added to the dictionary",imperial[i], sr.getFromDictionary(metricRegEx[i]));
		}
		
		for (int i =0; i < imperialRegEx.length; i++)
		{
			assertEquals("The key '"+ imperialRegEx[i] + "' with the value '" + imperial[i] +"' should be added to the dictionary",imperial[i], sr.getFromDictionary(imperialRegEx[i]));
		}
	}
	
	@Test
	public void testFRLocaleDictionary()
	{
		StringReplacer sr = new StringReplacer("�","euro","metric");
		assertEquals("The key '\\$Rs\\.\\$' with the value '�' should be added to the dictionary","�",sr.getFromDictionary("\\$Rs\\.\\$"));
		assertEquals("The key '\\$Rupees\\$' with the value 'euros' should be added to the dictionary","euros",sr.getFromDictionary("\\$Rupees\\$"));
		for (int i =0; i < imperialRegEx.length; i++)
		{
			assertEquals("The key '"+ imperialRegEx[i] + "' with the value '" + metric[i] +"' should be added to the dictionary",metric[i], sr.getFromDictionary(imperialRegEx[i]));
		}
		
		for (int i =0; i <  metricRegEx.length; i++)
		{
			assertEquals("The key '"+ metricRegEx[i] + "' with the value '" + metric[i] +"' should be added to the dictionary",metric[i], sr.getFromDictionary(metricRegEx[i]));
		}
	}
	
	@Test
	public void testConvertStringWithreplacement()
	{
		StringReplacer sr = new StringReplacer("\\$","dollar","imperial");
		RegExpMatching rx1 = new RegExpMatching("\\$Rs\\.\\$");
		RegExpMatching rx2 = new RegExpMatching("\\$Rupees\\.\\$");
		
		String test1 = "This sentence has $Rupees$ that need replacing $Rs.$";
		String test2 = "This sentence does not";
		String test3 = "This sentence has %kilometers% in it and %miles%";
		
		test1 = sr.convertStringWithreplacement(test1);
		test2 = sr.convertStringWithreplacement(test2);
		test3 = sr.convertStringWithreplacement(test3);
		
		assertFalse("Sentence has no rupees in it", rx1.expressionMatch(test1) || rx2.expressionMatch(test1));
		assertEquals("The new sentence should have had it's rupees replaced", "This sentence has dollars that need replacing $", test1);
		assertEquals("The new sentence should not have been changed", test2, test2);
		assertEquals("The new sentence should have had it's km replaced", "This sentence has miles in it and miles", test3);
	}
}
