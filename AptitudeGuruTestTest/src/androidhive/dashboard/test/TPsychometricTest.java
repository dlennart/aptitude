package androidhive.dashboard.test;

import static org.junit.Assert.*;

import org.hamcrest.Matcher;
//import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;

import com.aptitudeguru.dashboard.MyCountDownTimer;
import com.aptitudeguru.dashboard.Psychometric;
import com.aptitudeguru.dashboard.Quantitative;
import com.aptitudeguru.dashboard.ShowScore;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.test.ActivityUnitTestCase;
import android.view.View;
import android.widget.TextView;
public class TPsychometricTest extends ActivityUnitTestCase<Psychometric>{ 

	private Psychometric psychometricActivity;
	private MyCountDownTimer psychometricTimer;
	
	public TPsychometricTest()
	{
		super(Psychometric.class);
		
	}
	
	public TPsychometricTest(Class<Psychometric> activityClass) {
		super(activityClass);
		
	}
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		Intent intent = new Intent(getInstrumentation().getTargetContext(), Psychometric.class);
		intent.putExtra("developer", true);
		startActivity(intent, null, null);
		psychometricActivity = getActivity();
		psychometricTimer=psychometricActivity.CountDownTimerSet();
		
	}

	@Test
	public void testSetPageTextScenarioAndSituation() {
		psychometricActivity.setPagnation(0);
		CharSequence situation = "Situation 1\n\nA customer has been browsing in your section for about 10 minutes and is looking increasingly dissatisfied and frustrated. He approaches you and asks whether you have a particular book that he is looking for, and after checking on your computer, you have to inform him that it is currently 'out of stock'.";
		CharSequence scenario = "Scenario 1\n\nYou are a Retail Assistant working in the Saldringham branch of More Than Pens plc, a national stationer's chain. More Than Pens has over 1000 stores, primarily in the UK, including 451 travel outlets at airports, train stations and motorway service areas and 627 high street stores. More Than Pens sells a wide range of newspapers, magazines, books, stationery and impulse products; most branches are open 7 days a week between 8.30am and 5.30pm. As a Retail Assistant you are responsible for providing exceptional customer service whilst demonstrating product knowledge to maximise sales. You work primarily in the book section of the Saldringham branch, however all the branch staff work as a unified team and therefore you are often required to work in other sections throughout the store.";
		CharSequence answers = "Option A\nApologise that the book is unavailable and suggest he try Morethanpens.co.uk or another online retailer instead."
				+ "\n\nOption B\nOffer to order the book for the customer and let him know how long this will take. Offer to call him when the book arrives."
				+ "\n\nOption C\nGive the customer the ISBN (book serial number) of the book so that he can easily and quickly search it out elsewhere, either online or at another bookshop."
				+ "\n\nOption D\nSuggest he try the Alpston branch of More Than Pens (which is the nearest neighbouring town, 40 minutes drive away) or other bookshops in Saldringham.\n\n";

		
//		int gone = View.GONE;
		assertEquals("Text displyed should match scenario text",scenario + "\n\n" + situation + "\n\n" + answers,psychometricActivity.getMainTextDisplayText());
//		assertEquals("Answer Entry should be gone",gone,psychometricActivity.getAnswerEntryVisibility());
//		assertEquals("Answer Bar should be gone",gone,psychometricActivity.getAnswerBarVisibility());
//		assertEquals("Answer Labels should be gone",gone,psychometricActivity.getAnswerLabelsVisibility());
	}
	
	@Test
	public void testTimerStartTime()
	{
		assertEquals("Timer should be set to 5 minutes",psychometricTimer.getMinutes(), 5);
	}
	@Test 
	public void testTimerInitialised()
	{
		assertNotNull("Timer should be initialised", psychometricTimer);
	}
	
	@Test
	public void testAlertWasRun()
	{
		psychometricTimer.onTick(60000);
		assertTrue("The alert Method should be run at 1 minute and 0 seconds", psychometricTimer.getAlertRan());
	}
	
	@Test
	public void testTimerFormating()
	{
		psychometricTimer.onTick(61000);
		CharSequence expected = "Time left: 1:01";
		TextView timeDisplay = psychometricTimer.getTimeDisplay();
		assertEquals("The time should be displayed as Time left: 1:01", expected, timeDisplay.getText());
	}
}
